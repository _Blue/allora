from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
from .errors import *

MAC_SIZE_BYTES = 16
NONCE_SIZE_BYTES = 8

class EncryptionLayer:

    def __init__(self, key: bytes):
        assert len(key) == 16
        self.key = key

    def make_cipher(self, nonce: bytes) -> AES:
        return AES.new(self.key, AES.MODE_CCM, nonce=nonce, mac_len=MAC_SIZE_BYTES)

    def encrypt(self, payload: bytes) -> bytes:
        nonce = get_random_bytes(NONCE_SIZE_BYTES)
        ciphertext, tag = self.make_cipher(nonce).encrypt_and_digest(payload)

        assert len(nonce) == NONCE_SIZE_BYTES
        assert len(tag) == MAC_SIZE_BYTES
        return nonce + tag + ciphertext

    def decrypt(self, payload: bytes) -> bytes:
        if len(payload) < NONCE_SIZE_BYTES + MAC_SIZE_BYTES:
            raise MalformedPacketException(f'Message size is too small: {len(payload)}')


        nonce = payload[:NONCE_SIZE_BYTES]
        tag = payload[NONCE_SIZE_BYTES:NONCE_SIZE_BYTES + MAC_SIZE_BYTES]
        assert len(nonce) == NONCE_SIZE_BYTES
        assert len(tag) == MAC_SIZE_BYTES

        cipher = payload[NONCE_SIZE_BYTES + MAC_SIZE_BYTES:]

        try:
            return self.make_cipher(nonce).decrypt_and_verify(cipher, tag)
        except Exception as e:
            raise UnauthenticatedPacketException('Failed to decrypt and verify packet') from e

    @property
    def header_size(self) -> int:
        return NONCE_SIZE_BYTES + MAC_SIZE_BYTES

class NullEncryption(EncryptionLayer):
    def __init__(self):
        pass

    def encrypt(self, payload: bytes) -> bytes:
        return payload

    def decrypt(self, payload: bytes) -> bytes:
        return payload

    @property
    def header_size(self) -> int:
        return 0
