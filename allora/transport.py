import traceback
import random
import time
from dataclasses import dataclass
from typing import Tuple
from threading import Thread, Lock, Event
from queue import Queue, Empty, Full
from .packet import Parser, Packet, Builder
from .errors import *
from .utils import interruptible_queue_get, sleep_until
from . import logger

BROADCAST_ADDRESS = 15


class TransportRelay:
    def __init__(self, transport, encryption, relay_wait: float, relay_random_factor: float):
        self.transport = transport
        self.relay_wait = relay_wait
        self.relay_random_factor = relay_random_factor
        self.encryption = encryption
        self.queue = Queue(100)
        self.thread = None
        self.stop_event = Event()

    def start(self):
        assert self.thread is None
        assert not self.stop_event.is_set()

        logger.info(f'TransportRelay starting, relay_wait={self.relay_wait}, random_factor={self.relay_random_factor}')

        self.thread = Thread(target=self.run)
        self.thread.start()


    def stop(self):
        assert self.thread is not None
        assert not self.stop_event.is_set()

        logger.info('TransportRelay stopping')

        self.stop_event.set()
        self.thread.join()
        self.thread = None
        self.stop_event.clear()

    def run(self):
        while not self.stop_event.is_set():
            try:
                # Grab the first packet in queue
                packet, target_ts = interruptible_queue_get(self.queue, self.stop_event, None)
            except Empty: # Raise if the get times out
                continue

            try:
                # Wait until packet is ready to send
                sleep_until(target_ts)

                # Send the packet
                self.transport.schedule_send(packet)
                self.transport.relayed_packets += 1
            except:
                logger.error(f'Unexpected exception while relaying packet: {traceback.format_exc()}')
            finally:
                self.queue.task_done()

    def push(self, packet: Packet):
        assert packet.ttl > 0

        packet.ttl = packet.ttl - 1
        packet.flags = packet.flags | Packet.Flags.RELAYED.value

        # Schedule the packet to be sent at a specific time
        ts = time.time() + random.uniform(self.relay_wait, self.relay_wait + self.relay_random_factor)
        self.queue.put((self.encryption.encrypt(packet.content), ts))

    def join(self):
        self.queue.join()

    def __enter__(self):
        return self.start()

    def __exit__(self, *args, **kwargs):
        self.join()
        self.stop()


class Transport:

    @dataclass
    class Status:
        running: bool
        sent_packets: int
        received_packets: int
        corrupted_packets: int
        device_errors: int
        registered_callbacks: int
        unhandled_packets: int
        device_thread_running: bool
        dispatch_thread_running: bool
        receive_queue_size: int
        send_queue_size: int
        relayed_packets: int


    def __init__(
            self,
            device,
            encryption,
            address: int,
            receive_poll_time_seconds: float = 3,
            receive_poll_random_factor: float = 0.5,
            relay_packets = False,
            relay_wait: float = 0.5,
            relay_random_factor: float = 0.5,
            outgoing_ttl = 0):

        self.send_thread = None
        self.receive_thread = None
        self.lock = Lock()

        self.device = device
        self.running = False
        self.encryption = encryption
        self.packet_callbacks = []
        self.parser = Parser(self.encryption)
        self.builder = Builder(self.encryption)
        self.address = address
        self.interrupt_receive = Event()
        self.receive_poll_time_seconds = receive_poll_time_seconds
        self.receive_poll_random_factor = receive_poll_random_factor
        self.sent_packets = 0
        self.received_packets = 0
        self.corrupted_packets = 0
        self.device_errors = 0
        self.unhandled_packets = 0
        self.relayed_packets = 0
        self.outgoing_ttl = outgoing_ttl

        if relay_packets:
            self.relay = TransportRelay(self, encryption, relay_wait, relay_random_factor)
        else:
            self.relay = None

        assert self.address != BROADCAST_ADDRESS

        self.reset_queues()

    def reset_queues(self):
        self.send_queue = Queue(1000)
        self.receive_queue = Queue(1000)

    def register_packet_callback(self, callback):
        with self.lock:
            self.packet_callbacks.append(callback)

        return callback

    def unregister_packet_callback(self, callback):
        with self.lock:
            old_size = len(self.packet_callbacks)
            self.packet_callbacks = [e for e in self.packet_callbacks if not e is callback]
            new_size = len(self.packet_callbacks)

        if  old_size - new_size != 1:
            logger.warning(f'Unexpected callback list difference when unregistering callback : {old_size - new_size}')

    def status(self) -> Status:
        with self.lock:
            return Transport.Status(running = self.running,
                                    sent_packets = self.sent_packets,
                                    received_packets = self.received_packets,
                                    corrupted_packets = self.corrupted_packets,
                                    device_errors = self.device_errors,
                                    registered_callbacks = len(self.packet_callbacks),
                                    device_thread_running = self.send_thread is not None and self.send_thread.is_alive(),
                                    dispatch_thread_running = self.receive_thread is not None and self.receive_thread.is_alive(),
                                    receive_queue_size = self.receive_queue.qsize(),
                                    send_queue_size = self.send_queue.qsize(),
                                    unhandled_packets=self.unhandled_packets,
                                    relayed_packets=self.relayed_packets)

    @property
    def mtu(self):
        return (self.device.mtu - self.encryption.header_size) - 8 # L2 header is 8 bytes

    def start(self):
        assert self.send_thread is None and self.receive_thread is None and not self.running

        self.reset_queues()
        self.running = True

        self.send_thread = Thread(target=self.run)
        self.send_thread.start()

        self.receive_thread = Thread(target=self.dispatch)
        self.receive_thread.start()

        if self.relay is not None:
            self.relay.start()

    def stop(self):
        assert self.send_thread is not None and self.receive_thread is not None  and self.running
        self.running = False

        if self.relay is not None:
            self.relay.stop()

        try:
            self.receive_queue.put_nowait((None, None))
        except Full:
            logger.warning(f'Transport receive queue is full')

        self.interrupt_receive.set()

        self.send_thread.join()
        self.send_thread = None

        self.receive_thread.join()
        self.receive_thread = None

    def __enter__(self):
        return self.start()

    def __exit__(self, *args, **kwargs):
        self.stop()

    def sync(self):
        assert self.send_thread is not None and self.receive_thread is not None  and self.running
        self.receive_queue.join()

        if self.relay is not None:
            self.relay.join()

        self.send_queue.join()


    def run(self):
        logger.info(f'Transport starting')

        while self.running:
            if not self.send_queue.empty():
                send_payload = self.send_queue.get_nowait()
                self.send_impl(send_payload)
                self.send_queue.task_done()

            received_payload = self.receive_impl()
            if received_payload[0] is not None:
                if self.receive_queue.full():
                    logger.warning(f'Receive is queue is full, dropping packet: {received_payload}')
                else:
                    self.receive_queue.put(received_payload)

                continue


        logger.info('Transport exiting')

    def dispatch(self):
        logger.info(f'Transport dispatch starting')

        while self.running:
            payload, strength = self.receive_queue.get()

            # A None payload can be used to unblock this thread when the transport exits
            if payload is None:
                assert strength is None
                self.receive_queue.task_done()
                continue

            try:
                self.on_payload_received(payload, strength)
            finally:
                self.receive_queue.task_done()

        logger.info('Transport dispatch exiting')


    def on_payload_received(self, payload: bytes, signal_strength: int):
        # First parse the raw payload into a packet

        try:
            packet = self.parser.parse(payload)
            if signal_strength is not None:
                packet.set_signal_strength(signal_strength)

            self.received_packets += 1

            if packet.source == self.address:
                logger.debug(f'Received packet from ourselves ({self.address}), dropping')
                return

            relayed = False

            if packet.destination == BROADCAST_ADDRESS or (packet.source != self.address and packet.destination != self.address):
                if self.relay is not None and packet.ttl > 0:
                    logger.debug(f'Relaying packet from {packet.source} to {packet.destination}, ttl={packet.ttl}, relayed={packet.relayed}')
                    self.relay.push(packet)
                    relayed = True

            # Drop packet if it's not meant for us
            if packet.destination not in [BROADCAST_ADDRESS, self.address]:
                if not relayed:
                    logger.debug(f'Received packet for destination: {packet.destination} (ttl={packet.ttl}), dropping')

                return

        except:
            logger.warning(f'Failed to parse incoming packet: {payload}, {traceback.format_exc()}')
            self.corrupted_packets += 1
            return

        logger.debug('Dispatching received payload')
        # Then find a handler for the packet
        with self.lock:
            for e in self.packet_callbacks:
                try:
                    if e(packet):
                        # Packet has been handled, return.
                        return
                except:
                    logger.error(f'Error in packet handler, {traceback.format_exc()}')


        logger.warning(f'No packet handler found for packet: {packet}')
        self.unhandled_packets += 1

    def receive_impl(self) -> Tuple[bytes, int]:
        try:
            return self.device.receive(
                    random.uniform(self.receive_poll_time_seconds, self.receive_poll_time_seconds + self.receive_poll_random_factor),
                    self.interrupt_receive)
        except:
            logger.error(f'Device returned an error on receive() {traceback.format_exc()}')
            self.try_reset_device()
            self.device_errors += 1
            return None, None
        finally:
            # Only reset the interrupt event if the send queue is clear
            # So we don't wait a full receive timeout to send the next packet
            # (Calling device.receive() with a set event will not wait, but still returns content if a packet is pending)
            if self.send_queue.empty():
                self.interrupt_receive.clear()

    def try_reset_device(self):
        try:
            logger.info(f'Resetting device')
            self.device.initialize()
        except:
            logger.error(f'Failed to reset device, {traceback.format_exc()}')

    def send_impl(self, payload: bytes):

        try:
            self.device.send(payload)
            self.sent_packets += 1
        except:
            logger.error(f'Device returned an error on send(), {traceback.format_exc()}')
            self.try_reset_device()
            self.device_errors += 1

    def schedule_send(self, payload: bytes):
        self.send_queue.put(payload)
        self.interrupt_receive.set()

    def send(self, payload: bytes, destination: int, channel: int, flags: int):
        if not self.running:
            raise StoppedTransportException()

        if len(payload) > self.mtu:
            raise RuntimeError(f'Packet bigger than MTU ({self.mtu}) : {len(payload)}')

        packet = self.builder.build(payload, self.address, destination, channel, flags, self.outgoing_ttl)
        self.schedule_send(packet)
