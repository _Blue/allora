import adafruit_bus_device.spi_device as spidev
import time
from threading import Event
from .errors import DeviceError
from enum import Enum
from . import logger



class DeviceMode(Enum):
    SLEEP = 0b000
    STANDBY = 0b001
    FS_TX = 0b010
    TX = 0b011
    FS_RX = 0b100
    RX = 0b101

class DeviceRegister(Enum):
    FIFO = 0x00
    OP_MODE = 0x01
    FRF_MSB = 0x06
    FRF_MID = 0x07
    FRF_LSB = 0x08
    PA_CONFIG = 0x09
    PA_RAMP = 0x0A
    OCP = 0x0B
    LNA = 0x0C
    FIFO_ADDR_PTR = 0x0D
    FIFO_TX_BASE_ADDR = 0x0E
    FIFO_RX_BASE_ADDR = 0x0F
    FIFO_RX_CURRENT_ADDR = 0x10
    IRQ_FLAGS_MASK = 0x11
    IRQ_FLAGS = 0x12
    RX_NB_BYTES = 0x13
    RX_HEADER_CNT_VALUE_MSB = 0x14
    RX_HEADER_CNT_VALUE_LSB = 0x15
    RX_PACKET_CNT_VALUE_MSB = 0x16
    RX_PACKET_CNT_VALUE_LSB = 0x17
    MODEM_STAT = 0x18
    PKT_SNR_VALUE = 0x19
    PKT_RSSI_VALUE = 0x1A
    RSSI_VALUE = 0x1B
    HOP_CHANNEL = 0x1C
    MODEM_CONFIG1 = 0x1D
    MODEM_CONFIG2 = 0x1E
    SYMB_TIMEOUT_LSB = 0x1F
    PREAMBLE_MSB = 0x20
    PREAMBLE_LSB = 0x21
    PAYLOAD_LENGTH = 0x22
    MAX_PAYLOAD_LENGTH = 0x23
    HOP_PERIOD = 0x24
    FIFO_RX_BYTE_ADDR = 0x25
    MODEM_CONFIG3 = 0x26
    DIO_MAPPING1 = 0x40
    DIO_MAPPING2 = 0x41
    VERSION = 0x42
    TCXO = 0x4B
    PA_DAC = 0x4D
    FORMER_TEMP = 0x5B
    AGC_REF = 0x61
    AGC_THRESH1 = 0x62
    AGC_THRESH2 = 0x63
    AGC_THRESH3 = 0x64
    DETECTION_OPTIMIZE = 0x31
    DETECTION_THRESHOLD = 0x37

class DeviceConstants(Enum):
    PA_DAC_DISABLE = 0x04
    PA_DAC_ENABLE = 0x07

    #crystal oscillator frequency of the module
    FXOSC = 32000000.0
    FSTEP = FXOSC / 524288

BANDWIDTHS = [7800, 10400, 15600, 20800, 31250, 41700, 62500, 125000, 250000]

class RegisterBits:
    def __init__(self, address: DeviceRegister, *, offset=0, bits=1):
        assert 0 <= offset <= 7
        assert 1 <= bits <= 8
        assert (offset + bits) <= 8
        self._address = address
        self._mask = 0
        for _ in range(bits):
            self._mask <<= 1
            self._mask |= 1
        self._mask <<= offset
        self._offset = offset

    def __get__(self, obj, objtype):
        reg_value = obj._read_byte(self._address)
        return (reg_value & self._mask) >> self._offset

    def __set__(self, obj, val):
        reg_value = obj._read_byte(self._address)
        reg_value &= ~self._mask
        reg_value |= (val & 0xFF) << self._offset
        obj._write_byte(self._address, reg_value)


# Note: strongly inspired from https://github.com/adafruit/Adafruit_CircuitPython_RFM9x/blob/5832b25419ed592445bd2d8479a6164002da9ef5/adafruit_rfm9x.py
class Rfm9x:
    operation_mode = RegisterBits(DeviceRegister.OP_MODE, bits=3)
    low_frequency_mode = RegisterBits(DeviceRegister.OP_MODE, offset=3, bits=1)
    modulation_type = RegisterBits(DeviceRegister.OP_MODE, offset=5, bits=2)
    long_range_mode = RegisterBits(DeviceRegister.OP_MODE, offset=7, bits=1)
    output_power = RegisterBits(DeviceRegister.PA_CONFIG, bits=4)
    max_power = RegisterBits(DeviceRegister.PA_CONFIG, offset=4, bits=3)
    pa_select = RegisterBits(DeviceRegister.PA_CONFIG, offset=7, bits=1)
    pa_dac = RegisterBits(DeviceRegister.PA_DAC, bits=3)
    dio0_mapping = RegisterBits(DeviceRegister.DIO_MAPPING1, offset=6, bits=2)
    auto_agc = RegisterBits(DeviceRegister.MODEM_CONFIG3, offset=2, bits=1)
    low_datarate_optimize = RegisterBits(DeviceRegister.MODEM_CONFIG3, offset=3, bits=1)
    lna_boost_hf = RegisterBits(DeviceRegister.LNA, offset=0, bits=2)
    auto_ifon = RegisterBits(DeviceRegister.DETECTION_OPTIMIZE, offset=7, bits=1)
    detection_optimize = RegisterBits(DeviceRegister.DETECTION_OPTIMIZE, offset=0, bits=3)

    def __init__(
            self,
            spi,
            cs,
            reset,
            g0,
            frequency: int,
            preamble_length=8,
            baudrate=5000000,
            agc=True,
            crc=True,
            spreading_factor=7,
            tx_power=13,
            coding_rate=5,
            signal_bandwidth=125000,
            send_timeout=10,
            poll_rate=0.5,
            debug=False):

        self._device = spidev.SPIDevice(spi, cs, baudrate=baudrate, polarity=0, phase=0)
        self._g0 = g0
        self._buffer = bytearray(4)
        self._reset = reset
        self._reset.switch_to_output(value=True)
        self.send_timeout = send_timeout
        self.poll_rate = poll_rate
        self._frequency = frequency
        self._spreading_factor = spreading_factor
        self._preamble_length = preamble_length
        self._tx_power = tx_power
        self._baudrate = baudrate
        self._agc = agc
        self._crc = crc
        self._debug = debug
        self._coding_rate = coding_rate
        self._signal_bandwidth = signal_bandwidth
        self._interrupt_event = None
        self._interrupt_flags = 0
        self._local_event = Event()

        if self._g0 is not None:
            from RPi import GPIO
            GPIO.setmode(GPIO.BCM)
            GPIO.setup(self._g0, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
            GPIO.add_event_detect(self._g0, GPIO.RISING, callback=self._interrupt)

        self.initialize()

    def initialize(self):
        logger.info(f'Initializing device. frequency={self._frequency}, spreading_factor={self._spreading_factor}, tx_power={self._tx_power}, coding_rate={self._coding_rate}, signal_bandwidth={self._signal_bandwidth}, agc={self._agc}, crc={self._crc}, interrupts={self._g0 is not None}')

        self.reset()

        version = self._read_byte(DeviceRegister.VERSION)
        if version != 18:
            raise DeviceError(f'Unexpected device version: {version}')

        self._set_mode(DeviceMode.SLEEP)
        time.sleep(0.01)

        self.long_range_mode = True
        if self.operation_mode != DeviceMode.SLEEP.value or not self.long_range_mode:
            raise DeviceError("Failed to configure radio for LoRa mode.")

        # clear default setting for access to LF registers if frequency > 525MHz
        if self._frequency > 525:
            self.low_frequency_mode = 0

        # Setup entire 256 byte FIFO
        self._write_byte(DeviceRegister.FIFO_TX_BASE_ADDR, 0x00)
        self._write_byte(DeviceRegister.FIFO_RX_BASE_ADDR , 0x00)

        # Set mode idle
        self._set_mode(DeviceMode.STANDBY)

        # Set frequency
        self.frequency_mhz = self._frequency

        # Set preamble length (default 8 bytes to match radiohead).
        self.preamble_length = self._preamble_length

        # Defaults set modem config to RadioHead compatible Bw125Cr45Sf128 mode.
        self.signal_bandwidth = self._signal_bandwidth
        self.coding_rate = self._coding_rate
        self.spreading_factor = self._spreading_factor

        # Default to enable CRC checking on incoming packets.
        self.enable_crc = self._crc

        self.auto_agc = self._agc
        self.tx_power = self._tx_power

        # Set RX mode by default
        self._set_mode(DeviceMode.RX)

        def sanity_check(value: int, expected: int, name: str):
            if value != expected:
                raise DeviceError(f'Failed to initialize device register {name} with value {expected} (read {value})')

        sanity_check(self.long_range_mode, 1, 'long_range_mode')
        sanity_check(self.operation_mode, DeviceMode.RX.value, 'operation_mode')
        sanity_check(self.frequency_mhz, self._frequency, 'frequency')
        sanity_check(self.preamble_length, self._preamble_length, 'preamble_length')
        sanity_check(self.signal_bandwidth, self._signal_bandwidth, 'signal_bandwidth')
        sanity_check(self.coding_rate, self._coding_rate, 'coding_rate')
        sanity_check(self.spreading_factor, self._spreading_factor, 'spreading_factor')
        sanity_check(self.enable_crc, self._crc, 'enable_crc')
        sanity_check(self.auto_agc, self._agc, 'agc')
        sanity_check(self.tx_power, self._tx_power, 'tx_power')
        sanity_check(self.coding_rate, self._coding_rate, 'coding_rate')
        sanity_check(self.signal_bandwidth, self._signal_bandwidth, 'signal_bandwidth')


    @property
    def mtu(self):
        return 255

    def reset(self):
        self._reset.value = False  # Set Reset Low
        time.sleep(0.0001)  # 100 us
        self._reset.value = True  # set Reset High
        time.sleep(0.005)  # 5 ms

    def _set_mode(self, mode: DeviceMode):
        if self._debug:
            logger.debug(f'Setting device mode to {mode}')

        if mode == DeviceMode.RX:
            self.dio0_mapping = 0 # Interrupt on rx done.
        elif mode == DeviceMode.TX:
            self.dio0_mapping = 1 # Interrupt on tx done.

        self.operation_mode = mode.value

    def _read_into(self, address: DeviceRegister, buffer, length=None):
        # Read a number of bytes from the specified address into the provided
        # buffer.  If length is not specified (the default) the entire buffer
        # will be filled.
        if length is None:
            length = len(buffer)
        with self._device as device:
            self._buffer[0] = address.value & 0x7F  # Strip out top bit to set 0 to indicate a read
            device.write(self._buffer, end=1)
            device.readinto(buffer, end=length)

    def _write_from(self, address: DeviceRegister, buffer, length=None):
        # Write a number of bytes to the provided address and taken from the
        # provided buffer.  If no length is specified (the default) the entire
        # buffer is written.
        if length is None:
            length = len(buffer)
        with self._device as device:
            self._buffer[0] = (address.value | 0x80) & 0xFF  # Set top bit to 1 to indicate a write.
            device.write(self._buffer, end=1)
            device.write(buffer, end=length)

    def _read_byte(self, address: DeviceRegister) -> int:
        # Read a single byte from the provided address and return it.
        self._read_into(address, self._buffer, length=1)
        return self._buffer[0]

    def _write_byte(self, address: DeviceRegister, value: int):
        assert value >= 0 and value <= 255
        # 8-bit value to write to that address.
        with self._device as device:
            self._buffer[0] = ((address.value if isinstance(address, DeviceRegister) else address)| 0x80) & 0xFF  # Set top bit to 1 to indicate a write.
            self._buffer[1] = value
            device.write(self._buffer, end=2)

    @property
    def preamble_length(self):
        """The length of the preamble for sent and received packets, an unsigned
        16-bit value.  Received packets must match this length or they are
        ignored! Set to 8 to match the RadioHead RFM95 library.
        """
        msb = self._read_byte(DeviceRegister.PREAMBLE_MSB)
        lsb = self._read_byte(DeviceRegister.PREAMBLE_LSB)
        return ((msb << 8) | lsb) & 0xFFFF

    @preamble_length.setter
    def preamble_length(self, value: int):
        assert 0 <= value <= 65535
        self._write_byte(DeviceRegister.PREAMBLE_MSB, (value >> 8) & 0xFF)
        self._write_byte(DeviceRegister.PREAMBLE_LSB, value & 0xFF)

    @property
    def frequency_mhz(self):
        """The frequency of the radio in Megahertz. Only the allowed values for
        your radio must be specified (i.e. 433 vs. 915 mhz)!
        """
        msb = self._read_byte(DeviceRegister.FRF_MSB)
        mid = self._read_byte(DeviceRegister.FRF_MID)
        lsb = self._read_byte(DeviceRegister.FRF_LSB)
        frf = ((msb << 16) | (mid << 8) | lsb) & 0xFFFFFF
        frequency = (frf * DeviceConstants.FSTEP.value) / 1000000.0
        return frequency

    @frequency_mhz.setter
    def frequency_mhz(self, val):
        if val < 240 or val > 960:
            raise RuntimeError("frequency_mhz must be between 240 and 960")
        # Calculate FRF register 24-bit value.
        frf = int((val * 1000000.0) / DeviceConstants.FSTEP.value) & 0xFFFFFF
        # Extract byte values and update registers.
        msb = frf >> 16
        mid = (frf >> 8) & 0xFF
        lsb = frf & 0xFF
        self._write_byte(DeviceRegister.FRF_MSB, msb)
        self._write_byte(DeviceRegister.FRF_MID, mid)
        self._write_byte(DeviceRegister.FRF_LSB, lsb)

    @property
    def tx_power(self):
        """The transmit power in dBm. Can be set to a value from 5 to 23 for
        high power devices (RFM95/96/97/98, high_power=True) or -1 to 14 for low
        power devices.
        """
        return self.output_power + (8 if self.pa_dac == DeviceConstants.PA_DAC_ENABLE.value else 5)

    @tx_power.setter
    def tx_power(self, value: int):
        if value < 5 or value > 23:
            raise RuntimeError("tx_power must be between 5 and 23")
        # Enable power amp DAC if power is above 20 dB.
        # Lower setting by 3db when PA_BOOST enabled - see Data Sheet  Section 6.4
        if value > 20:
            self.pa_dac = DeviceConstants.PA_DAC_ENABLE.value
            assert self.pa_dac == DeviceConstants.PA_DAC_ENABLE.value
            value -= 3
        else:
            self.pa_dac = DeviceConstants.PA_DAC_DISABLE.value

        self.pa_select = True
        assert self.pa_select

        self.output_power = (value - 5) & 0x0F

    @property
    def rssi(self):
        """The received strength indicator (in dBm) of the last received message."""
        # Read RSSI register and convert to value using formula in datasheet.
        # Remember in LoRa mode the payload register changes function to RSSI!
        raw_rssi = self._read_byte(DeviceRegister.PKT_RSSI_VALUE)
        if self.low_frequency_mode:
            raw_rssi -= 157
        else:
            raw_rssi -= 164
        return raw_rssi

    @property
    def snr(self):
        """The SNR (in dB) of the last received message."""
        # Read SNR 0x19 register and convert to value using formula in datasheet.
        # SNR(dB) = PacketSnr [twos complement] / 4
        snr_byte = self._read_byte(DeviceRegister.PKT_SNR_VALUE)
        if snr_byte > 127:
            snr_byte = (256 - snr_byte) * -1
        return snr_byte / 4

    @property
    def signal_bandwidth(self):
        """The signal bandwidth used by the radio (try setting to a higher
        value to increase throughput or to a lower value to increase the
        likelihood of successfully received payloads).  Valid values are
        listed in BANDWIDTHS."""
        bw_id = (self._read_byte(DeviceRegister.MODEM_CONFIG1) & 0xF0) >> 4
        if bw_id >= len(BANDWIDTHS):
            current_bandwidth = 500000
        else:
            current_bandwidth = BANDWIDTHS[bw_id]
        return current_bandwidth

    @signal_bandwidth.setter
    def signal_bandwidth(self, val):
        for bw_id, cutoff in enumerate(BANDWIDTHS):
            if val == cutoff:
                break
        else:
            raise DeviceError(f'Frequency not supported: {val}')

        self._write_byte(DeviceRegister.MODEM_CONFIG1, (self._read_byte(DeviceRegister.MODEM_CONFIG1) & 0x0F) | (bw_id << 4))
        if val >= 500000:
            # see Semtech SX1276 errata note 2.3
            self.auto_ifon = True
            # see Semtech SX1276 errata note 2.1
            if self.low_frequency_mode:
                self._write_byte(0x36, 0x02)
                self._write_byte(0x3A, 0x7F)
            else:
                self._write_byte(0x36, 0x02)
                self._write_byte(0x3A, 0x64)
        else:
            # see Semtech SX1276 errata note 2.3
            self.auto_ifon = False
            self._write_byte(0x36, 0x03)
            if val == 7800:
                self._write_byte(0x2F, 0x48)
            elif val >= 62500:
                # see Semtech SX1276 errata note 2.3
                self._write_byte(0x2F, 0x40)
            else:
                self._write_byte(0x2F, 0x44)
            self._write_byte(0x30, 0)

    @property
    def coding_rate(self):
        """The coding rate used by the radio to control forward error
        correction (try setting to a higher value to increase tolerance of
        short bursts of interference or to a lower value to increase bit
        rate).  Valid values are limited to 5, 6, 7, or 8."""
        cr_id = (self._read_byte(DeviceRegister.MODEM_CONFIG1) & 0x0E) >> 1
        denominator = cr_id + 4
        return denominator

    @coding_rate.setter
    def coding_rate(self, val):
        # Set coding rate (set to 5 to match RadioHead Cr45).
        denominator = min(max(val, 5), 8)
        cr_id = denominator - 4
        self._write_byte(
            DeviceRegister.MODEM_CONFIG1,
            (self._read_byte(DeviceRegister.MODEM_CONFIG1) & 0xF1) | (cr_id << 1),
        )

    @property
    def spreading_factor(self):
        """The spreading factor used by the radio (try setting to a higher
        value to increase the receiver's ability to distinguish signal from
        noise or to a lower value to increase the data transmission rate).
        Valid values are limited to 6, 7, 8, 9, 10, 11, or 12."""
        sf_id = (self._read_byte(DeviceRegister.MODEM_CONFIG2) & 0xF0) >> 4
        return sf_id

    @spreading_factor.setter
    def spreading_factor(self, val):
        # Set spreading factor (set to 7 to match RadioHead Sf128).
        val = min(max(val, 6), 12)

        if val == 6:
            self.detection_optimize = 0x5
        else:
            self.detection_optimize = 0x3

        self._write_byte(DeviceRegister.DETECTION_THRESHOLD, 0x0C if val == 6 else 0x0A)
        self._write_byte(
            DeviceRegister.MODEM_CONFIG2,
            (
                (self._read_byte(DeviceRegister.MODEM_CONFIG2) & 0x0F)
                | ((val << 4) & 0xF0)
            ),
        )

    @property
    def enable_crc(self):
        """Set to True to enable hardware CRC checking of incoming packets.
        Incoming packets that fail the CRC check are not processed.  Set to
        False to disable CRC checking and process all incoming packets."""
        return (self._read_byte(DeviceRegister.MODEM_CONFIG2) & 0x04) == 0x04

    @enable_crc.setter
    def enable_crc(self, value: bool):
        if value:
            self._write_byte(DeviceRegister.MODEM_CONFIG2, self._read_byte(DeviceRegister.MODEM_CONFIG2) | 0x04)
        else:
            self._write_byte(DeviceRegister.MODEM_CONFIG2, self._read_byte(DeviceRegister.MODEM_CONFIG2) & 0xFB)

    def tx_done(self):
        return (self._read_byte(DeviceRegister.IRQ_FLAGS) & 0x8) >> 3

    def rx_done(self):
        return (self._read_byte(DeviceRegister.IRQ_FLAGS) & 0x40) >> 6

    def crc_error(self):
        return (self._read_byte(DeviceRegister.IRQ_FLAGS) & 0x20) >> 5

    def _interrupt(self, channel):
        if self._debug:
            logger.debug(f'Received interrupt, event = {self._interrupt_event is not None}')

        event = self._interrupt_event
        if event is not None:
            event.set()

            if event is self._local_event:
                event.clear()

    def send(self, payload: bytes):
        if len(payload) > 255:
            raise DeviceError(f'Payload too big for device ({len(payload)} bytes)')

        # Set interrupt_event

        self._interrupt_event = self._local_event

        # Clear fifo
        self._set_mode(DeviceMode.STANDBY)

        # Start FIFO
        self._write_byte(DeviceRegister.FIFO_ADDR_PTR, 0)

        # Write payload
        self._write_from(DeviceRegister.FIFO, payload)
        self._write_byte(DeviceRegister.PAYLOAD_LENGTH, len(payload))

        # Transmit
        self._set_mode(DeviceMode.TX)

        if self._debug:
            assert self._read_byte(DeviceRegister.PAYLOAD_LENGTH) == len(payload)

        # Wait for transmission to be complete
        deadline = time.time() + self.send_timeout
        try:
            while not self.tx_done():
                self._interrupt_event.wait(self.poll_rate)
                if self.tx_done():
                    break

                elif time.time() > deadline:
                    raise DeviceError(f'Timed out sending payload')

        finally:
            # Clear interupts
            self._write_byte(DeviceRegister.IRQ_FLAGS, 0xFF)
            self._interrupt_event = None

            # Go back to listen mode
            self._set_mode(DeviceMode.RX)

    def receive(self, timeout_seconds: float, interupt_event: Event=None):
        # Below assertion is debug-only for performance reasons
        if self._debug and self.operation_mode != DeviceMode.RX.value:
            raise DeviceError(f'Unexpected device mode for listen(): {self.operation_mode}')

        deadline = time.time() + timeout_seconds

        self._interrupt_event = interupt_event or self._local_event
        while not self.rx_done():
            if self._interrupt_event.wait(self.poll_rate):
                if self.rx_done():
                    break
                else:
                    return None, None # Interrupt event triggered

            if time.time() > deadline: # Timeout
                return None, None

        try:
            fifo_length = self._read_byte(DeviceRegister.RX_NB_BYTES)
            if fifo_length > 0:  # read and clear the FIFO if anything in it
                current_addr = self._read_byte(DeviceRegister.FIFO_RX_CURRENT_ADDR)
                self._write_byte(DeviceRegister.FIFO_ADDR_PTR, current_addr)
                packet = bytearray(fifo_length)

                self._read_into(DeviceRegister.FIFO, packet)
                return packet, self.rssi
            else:
                return bytearray(0), self.rssi
        finally:
            # Clear interrupts
            self._write_byte(DeviceRegister.IRQ_FLAGS, 0xFF)
            self._interrupt_event = None
