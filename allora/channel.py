import traceback
import random
import copy
from queue import Queue, Empty
from datetime import datetime, timedelta
from threading import Thread, get_ident, Event, Lock
from .packet import OrderedPacketType, ConnectPacket, AckPacket, DataPacket, Packet, ORDERED_MAGIC, FragmentedDataPacket
from .transport import Transport, BROADCAST_ADDRESS
from .utils import interruptible_queue_get
from .errors import *
from . import logger
from enum import Enum
from dataclasses import dataclass

class Channel:
    def __init__(self, transport: Transport, channel_id: int, destination: int):
        self.transport = transport
        self.id = channel_id
        self.destination = destination
        self.receive_callback = None
        self.receive_thread = None
        self.running = False
        self.receive_queue = Queue(1000)

    def set_receive_callback(self, callback):
        self.receive_callback = callback

    def __enter__(self, *args, **kwargs):
        return self.start(*args, **kwargs)

    def __exit__(self, *args, **kwargs):
        if self.running:
            self.stop()

    def start(self):
        assert self.receive_thread is None and not self.running

        self.transport_callback = self.transport.register_packet_callback(self.on_receive)
        self.running = True
        self.receive_thread = Thread(target=self.run)
        self.receive_thread.start()

    def stop(self):
        assert self.receive_thread is not None and self.running

        self.stop_impl()

    def stop_impl(self):
        logger.info(f'Scheduling stop in channel {self.id}')

        self.running = False
        self.transport.unregister_packet_callback(self.transport_callback)
        self.transport_callback = None
        self.receive_queue.put(None)

        if get_ident() != self.receive_thread.ident:
            self.receive_thread.join()

            # TODO: Detach thread cleanly
        self.receive_thread = None

        # Make sure that the queue is empty when leaving
        while not self.receive_queue.empty():
            logger.warning(f'Channel {self.id} is stopped. Discarding: {self.receive_queue.get()}')
            self.receive_queue.task_done()


    def sync(self):
        if not self.running:
            raise StoppedChannelException()

        self.transport.sync()
        self.receive_queue.join()


class BroadcastChannel(Channel):
    @dataclass
    class Stats:
        sent_packets: int
        received_packets: int

    def __init__(self, transport: Transport, id: int):
        self.sent_packets = 0
        self.received_packets = 0

        super().__init__(transport, id, BROADCAST_ADDRESS)

    def on_receive(self, packet: Packet) -> bool:
        if packet.channel == self.id and packet.destination == BROADCAST_ADDRESS:
            self.receive_queue.put(packet)
            return True

        return False

    def run(self):
        logger.info(f'Channel {self.id} starting')

        while self.running:
            packet = self.receive_queue.get()

            # None packet can be used to unblock the loop when stopping
            if packet is None:
                continue


            self.received_packets += 1
            if self.receive_callback:
                try:
                    self.receive_callback(packet)
                except:
                    logger.error(f'Unhandled exception in channel {self.id}, {traceback.format_exc()}')

            self.receive_queue.task_done()

    def send(self, payload: bytes):
        if not self.running:
            raise StoppedChannelException()

        self.sent_packets += 1
        return self.transport.send(payload, BROADCAST_ADDRESS, self.id, 0)

    def mtu(self):
        return self.transport.mtu # No extra header for Broadcast channels

    def statistics(self):
        return BroadcastChannel.Stats(sent_packets = self.sent_packets, received_packets = self.received_packets)


class OrderedChannel(Channel):
    class Stats:
        def __init__(self):
            self.sent_data_packets = 0
            self.received_data_packets = 0
            self.sent_acks = 0
            self.received_acks = 0
            self.sent_connect_packets = 0
            self.received_connect_packets = 0
            self.total_ack_wait = timedelta(seconds=0)
            self.ack_wait_count = 0
            self.repeated_ack_packets = 0
            self.repeated_data_packets = 0
            self.current_ack_wait_start = None

        def start_ack_wait(self):
            assert self.current_ack_wait_start is None
            self.current_ack_wait_start = datetime.now()

        def end_ack_wait(self):
            assert self.current_ack_wait_start is not None

            self.total_ack_wait += datetime.now() - self.current_ack_wait_start
            self.ack_wait_count += 1
            self.current_ack_wait_start = None

        def cancel_ack_wait(self):
            assert self.current_ack_wait_start is not None
            self.current_ack_wait_start = None

        @property
        def average_ack_time(self) -> timedelta:
            if self.ack_wait_count == 0:
                return 0

            return self.total_ack_wait / self.ack_wait_count

        def __repr__(self) -> str:
            return f'''
- Data packets:
  - sent: {self.sent_data_packets}
  - received: {self.received_data_packets}
- Ack packets:
  - sent: {self.sent_acks}
  - received: {self.received_acks}
- Connect packets:
  - sent: {self.sent_connect_packets}
  - received: {self.received_connect_packets}
- Connection quality:
  - Average ack time: {self.average_ack_time}
  - Repeated acks: {self.repeated_ack_packets}
  - Repeated data packets: {self.repeated_data_packets}'''

    class State(Enum):
        CONNECTING = 1
        CONNECTED = 2
        DISCONNECTED = 3

    class Transaction:
        def __init__(self, payload: bytes, seq_number: int, mtu: int, ack_timeout: datetime, disconnect_timeout: datetime, callback):
            self.first_seq_number = seq_number
            self.fragment_index = 0
            self.ack_timeout = ack_timeout
            self.disconnect_timeout = disconnect_timeout
            self.error = None
            self.completed_event = Event()
            self.callback = callback

            # Split payload into fragments if needed
            self.fragments = []
            while len(payload) > 0:
                self.fragments.append(payload[:mtu])
                payload = payload[mtu:]

            if len(self.fragments) == 0:
                self.fragments = [bytes()] # In case the payload was already empty

        def on_ack(self, seq_number: int):
            if seq_number == self.current_seq_number():
                # Force a reset of the timeouts on next fragment
                self.ack_timeout = None
                self.disconnect_timeout = None
                self.fragment_index += 1
                return True
            else:
                logger.warning(f'Transaction received unexpected seq_number={seq_number}, expected {self.current_seq_number()}')
                return False

        def restart(self, new_seq_number: int):
            # Called when the channel reconnected while the transaction was pending

            assert not self.completed_event.is_set()
            assert self.error is None

            logger.debug(f'Restarting transaction with seq_number={self.current_seq_number()} on seq_number={new_seq_number}')
            self.fragment_index = 0
            self.first_seq_number = new_seq_number
            self.ack_timeout = None

        def is_complete(self) -> bool:
            return self.fragment_index >= len(self.fragments)

        def current_flags(self) -> int:
            if self.fragment_index == len(self.fragments) - 1:
                return 0
            elif self.fragment_index < len(self.fragments) - 1:
                return Packet.Flags.FRAGMENTED.value
            else:
                RuntimeError(f'No current fragment in transaction')

        def current_fragment(self) -> bytes:
            return  self.current_seq_number().to_bytes(4, 'big') + self.fragments[self.fragment_index]

        def current_seq_number(self) -> int:
            return self.fragment_index + self.first_seq_number

        def last_seq_number(self) -> int:
            return self.first_seq_number + len(self.fragments) - 1

        def complete(self, error: Exception):
            self.error = error
            self.completed_event.set()
            if self.callback is not None:
                self.callback(error)
                self.callback = None # Shouldn't be needed, but better be safe

        def wait(self):
            self.completed_event.wait()

            if self.error:
                raise self.error

    def __init__(
            self,
            transport,
            id: int,
            destination: int,
            connect_timeout: float,
            ack_timeout: float,
            disconnect_timeout: float,
            connect_retry: float = 1.25,
            receive_poll: float= 0.5,
            random_factor: float = 0.7):
        super().__init__(transport, id, destination)

        self.ack_timeout = timedelta(seconds=ack_timeout)
        self.connect_timeout = timedelta(seconds=connect_timeout) if connect_timeout else None
        self.disconnect_timeout = timedelta(seconds=disconnect_timeout)
        self.connect_retry = timedelta(seconds=connect_retry)
        self.state = OrderedChannel.State.DISCONNECTED
        self.pending_transaction = None
        self.connected_event = Event()
        self.local_seq_number = 0
        self.remote_seq_number = 0
        self.connect_error = None
        self.transaction_lock = Lock()
        self.interrupt_receive = Event()
        self.connection_id = random.randint(1, 2 ** 32 - 1)
        self.receive_poll = receive_poll
        self.random_factor = random_factor
        self.stats = OrderedChannel.Stats()

    def on_receive(self, packet: Packet) -> bool:
        if packet.channel == self.id and packet.source == self.destination:
            self.receive_queue.put(packet)
            return True

        return False


    def set_seq_numbers(self, local: int, remote: int):
        # Edge case: If the channel disconnected right after we sent a data packet
        # And the other end sent an ACK packet that we didn't receive, it's possible
        # that the other end has a remote_seq_number == self.local_seq_number + 1.
        # In that case, complete the pending transaction

        if local == self.local_seq_number + 1:
            if self.pending_transaction is None:
                logger.warning(f'Remote seq_number matches ours + 1, but no pending transaction is in progress')
            else:
                logger.debug(f'Remote seq_number matches ours + 1, continuing transaction {self.pending_transaction.current_seq_number}')
                self.on_transaction_ack(local)

        self.local_seq_number = local

        # We also need to handle the 'other side' of the above edge case.
        # If the other side's local_seq_number == self.remote_seq_number - 1
        # That means that we sent an ack packet that was not received.
        # Because the other side will adjust its seq number, we need to do the same to make sure they match

        if remote > 0 and remote - 1 == self.remote_seq_number:
            logger.warning(f'Adjusting remote_seq_number to {remote + 1} to match lost ACK packet')
            self.remote_seq_number = remote + 1
        else:
            self.remote_seq_number = remote

    def start(self, connect_callback=None):
        self.connect_callback = connect_callback

        self.reset_connected_state()

        super().start()

    def state(self):
        return self.state

    def statistics(self) -> Stats:
        return copy.deepcopy(self.stats)

    def transition(self, new_state: State):
        logger.debug(f'Channel {self.id} transition {self.state} -> {new_state}')
        self.state = new_state

    def reset_connected_state(self):
        self.connect_error = None
        self.connected_event.clear()
        self.pending_receive_fragment = None

    def examine_pending_transaction(self):
        assert self.pending_transaction is not None

        now = datetime.now()
        if self.pending_transaction.ack_timeout is None:
            logger.debug(f'Sending packet {self.pending_transaction.current_seq_number()} on channel {self.id}')
            self.stats.start_ack_wait()
            self.pending_transaction.ack_timeout = now + self.ack_timeout
            if self.pending_transaction.disconnect_timeout is None:
                self.pending_transaction.disconnect_timeout = now + self.disconnect_timeout

            self.send_packet_impl(OrderedPacketType.DATA, self.pending_transaction.current_fragment(), self.pending_transaction.current_flags())

        elif now > self.pending_transaction.disconnect_timeout:
            logger.info(f'Transaction timed out on packet {self.pending_transaction.current_seq_number()} on channel {self.id}')
            self.reset_connected_state()
            transaction = self.pending_transaction
            self.pending_transaction = None
            transaction.complete(TransactionTimeout())

            self.stats.cancel_ack_wait()
            # Pending transaction timed out, reconnect
            if not self.connect():
                return False

        elif now > self.pending_transaction.ack_timeout:
            # Pending transaction ack timed out, resend
            self.stats.repeated_data_packets += 1
            self.pending_transaction.ack_timeout = now + self.ack_timeout
            logger.info(f'Resending packet {self.pending_transaction.current_seq_number()} on channel {self.id}. New ACK timeout: {self.pending_transaction.ack_timeout}')
            self.send_packet_impl(OrderedPacketType.DATA, self.pending_transaction.current_fragment(), self.pending_transaction.current_flags())

        return True

    def run(self):
        logger.info(f'Channel {self.id} starting')

        if not self.connect():
            return

        while self.running:
            with self.transaction_lock:
                if self.pending_transaction is not None:
                    # If we have a pending transaction, and no ACK was received, repeat the transaction

                    try:
                        if not self.examine_pending_transaction():
                            logger.info(f'Transaction timed out, stopping channel {self.id}')
                            break
                    except:
                        logger.error(f'Unexpected error caught while examinating pending transaction: {traceback.format_exc()}')

            try:
                packet = interruptible_queue_get(self.receive_queue, self.interrupt_receive, timeout_seconds=self.receive_poll + random.uniform(0, self.random_factor))
            except Empty:
                continue
            finally:
                self.interrupt_receive.clear()

            try:
                if packet is None or not self.on_packet(packet):
                    logger.warning(f'Stopping channel after packet {packet}')
                    break

            except Exception:
                logger.error(f'Unexpected exception while handling packet on channel {self.id}, {traceback.format_exc()}')
            finally:
                self.receive_queue.task_done()

        self.transition(OrderedChannel.State.DISCONNECTED)
        logger.info(f'Channel {self.id} stopping')


    def send_packet_impl(self, packet_type: OrderedPacketType, content: bytes, flags=0):
        payload = packet_type.value.to_bytes(2, 'big') + content
        self.transport.send(payload, self.destination, self.id, flags)

        if packet_type == OrderedPacketType.CONTROL:
            self.stats.sent_connect_packets += 1
        elif packet_type == OrderedPacketType.DATA:
            self.stats.sent_data_packets += 1
        elif packet_type == OrderedPacketType.ACK:
            self.stats.sent_acks += 1
        else:
            raise RuntimeError(f'Unexpected packet type: {packet_type}')

    def send_packet_ack(self, seq_number: int):
        logger.debug(f'Sending ACK for seq_number={seq_number}')
        self.send_packet_impl(OrderedPacketType.ACK, seq_number.to_bytes(4, 'big'))

    def send_connect_packet(self, reply: bool):
        connect_payload = (ORDERED_MAGIC.to_bytes(2, 'big') +
                           self.connection_id.to_bytes(4, 'big') +
                           self.local_seq_number.to_bytes(4, 'big') +
                           self.remote_seq_number.to_bytes(4, 'big'))

        self.send_packet_impl(OrderedPacketType.CONTROL, connect_payload, Packet.Flags.REPLY.value if reply else 0)

    def on_packet(self, packet: Packet):
        if len(packet.payload) < 2:
            logger.warning(f'Channel {self.id} dropping packet (below 2 bytes)')
            return True # Packet too small, drop

        parsed_packet = self.transport.parser.parse_ordered(packet)
        if parsed_packet.packet_type == OrderedPacketType.CONTROL:
            self.stats.received_connect_packets += 1
            if not self.on_control_packet(parsed_packet):
                return False
        elif parsed_packet.packet_type == OrderedPacketType.DATA:
            self.stats.received_data_packets += 1
            self.on_data_packet(parsed_packet)
        elif parsed_packet.packet_type == OrderedPacketType.ACK:
            self.stats.received_acks += 1
            self.on_ack_packet(parsed_packet)
        else:
            logger.warning(f'Channel {self.id} received packet with unexpected type: {parsed_packet.packet_type}')

        return True


    def on_control_packet(self, packet: ConnectPacket) -> bool:
        try:
            if packet.connect_id == self.connection_id:
                logger.debug(f'Channel {self.id} received a matching connect packet in connected state. Reply={packet.is_reply}')
                if not packet.is_reply:
                    self.send_connect_packet(True)
                return True
            else:
                logger.warning(f'Channel {self.id} received a connect packet with a new id ({packet.connect_id}). Reconnecting')
                self.reset_connected_state()
                return self.connect()
        except Exception:
            logger.warning(f'Error while processing connect packet in channel {self.id}, {traceback.format_exc()}')
            return True

    def on_data_packet(self, packet: DataPacket):
        # Acknoledge packet before continuing

        if packet.seq_number == self.remote_seq_number:
            logger.warning(f'Received already acknowledged packet on channel {self.id} (seq_number={packet.seq_number}). Sending ACK again')
            self.stats.repeated_ack_packets += 1
            self.send_packet_ack(packet.seq_number)
            return
        elif packet.seq_number - 1 != self.remote_seq_number:
            logger.warning(f'Unexpected remote seq number after {self.remote_seq_number}, {packet.seq_number}, ignoring')
            return
        else:
            logger.debug(f'Received data packet on channel {self.id} seq_number={packet.seq_number}, fragmented={packet.fragmented}')

        self.remote_seq_number = packet.seq_number
        self.send_packet_ack(packet.seq_number)

        if self.pending_receive_fragment is not None:
            self.pending_receive_fragment.add_fragment(packet)
            if not self.pending_receive_fragment.is_complete():
                return
            else:
                packet = self.pending_receive_fragment
                self.pending_receive_fragment = None
        elif packet.fragmented:
            self.pending_receive_fragment = FragmentedDataPacket(packet)
            return

        if self.receive_callback:
            try:
                self.receive_callback(packet)
            except:
                logger.error(f'Unhandled exception in channel {self.id}, {traceback.format_exc()}')

    def on_transaction_ack(self, seq_number: int):
        if self.pending_transaction.on_ack(seq_number):
            self.stats.end_ack_wait() # Record the time of when the ack is received

            logger.info(f'Received ACK on channel {self.id} for seq_number={seq_number}')
            self.local_seq_number = seq_number

            if self.pending_transaction.is_complete():
                logger.info(f'Completed transaction with seq_number={seq_number} on channel {self.id}')
                transaction = self.pending_transaction
                self.pending_transaction = None
                transaction.complete(None)
                self.local_seq_number = transaction.last_seq_number()
        else:
            logger.warning(f'ACK packet does not match pending transaction seq number (packet has {seq_number}, pending transaction has {self.pending_transaction.current_seq_number()})')

    def on_ack_packet(self, packet: AckPacket):
        try:
            if self.pending_transaction is None:
                # No pending transaction, we don't care about an ACK packet right now
                return
            else:
                self.on_transaction_ack(packet.seq_number)

        except:
            logger.warning(f'Error while parsing ACK packet, {traceback.format_exc()}')


    def connect(self):
        self.transition(OrderedChannel.State.CONNECTING)
        assert self.connect_error is None
        assert not self.connected_event.is_set()

        try:
            self.connect_impl()
        except Exception as e:
            logger.info(f'Channel {self.id} failed to connect, {traceback.format_exc()}')
            self.connect_error = e

        if self.connect_callback:
            self.connect_callback(self.connect_error)

        # Special case for when a disconnect occured while a transaction was pending.
        # In that case reset the transaction with the new seq_number

        if self.pending_transaction is not None and self.pending_transaction.current_seq_number() != self.local_seq_number + 1:
            logger.debug(f'Transaction seq_number out of sync after connect, restarting transaction')
            self.stats.cancel_ack_wait()
            self.pending_transaction.restart(self.local_seq_number + 1)

        self.connected_event.set()
        if self.connect_error is not None:
            return False

        self.transition(OrderedChannel.State.CONNECTED)
        return True

    def connect_impl(self):

        deadline = (datetime.now() + self.connect_timeout) if self.connect_timeout is not None else None
        logger.info(f'Channel {self.id} connecting, deadline={deadline}')


        while (deadline is None or datetime.now() < deadline) and self.running:
            self.send_connect_packet(False)

            try:
                packet = self.receive_queue.get(timeout=self.connect_retry.seconds + random.uniform(0, self.random_factor))
            except Empty:
                continue

            try:
                if packet is None:
                    continue

                parsed_packet = self.transport.parser.parse_ordered(packet)

                if parsed_packet.packet_type != OrderedPacketType.CONTROL:
                    logger.warning(f'Unexpected packet type after connect: {parsed_packet.packet_type}, ignoring')
                    continue

                remote_id = parsed_packet.connect_id

                # Multiple scenarios:
                # a) We receive a packet with matching id
                # b) We receive a packet with > id
                # c) We receive a packet with a < id

                if remote_id == self.connection_id:
                    logger.debug(f'Channel {self.id} received matching connection id: {self.connection_id}')

                    self.set_seq_numbers(parsed_packet.remote_seq_number, parsed_packet.local_seq_number)
                    # a) We're done
                    return
                elif remote_id > self.connection_id:

                    # b) The remote id is higher, so use that and retry
                    self.connection_id = remote_id

                    # If the remote connection id is different than the previous one, it means
                    # that this is a 'brand new' connection, so no state is carried over from the previous one.
                    # Clear the sequence numbers.

                    self.set_seq_numbers(0, 0)
                    logger.debug(f'Channel {self.id} switching to remote connection id: {remote_id}')
                    continue

                else:

                    logger.debug(f'Channel {self.id} receive lower remote connection id ({remote_id}), waiting for next packet')

                    # Reset the seq numbers since the remote is using a new connection id
                    self.set_seq_numbers(0, 0)

                    # c) Remote id should change, retry
                    continue
            except:
                logger.warning(f'Error while parsing connect packet: {traceback.format_exc()}')
                return False
            finally:
                self.receive_queue.task_done()

        if not self.running: # Case where the transport was stopped during a connection attempt
            raise StoppedChannelException('Channel was stopped during connect()')
        else:
            raise TimeoutError('Connect() timed out')

    def fragment_mtu(self):
        return self.transport.mtu - 6

    def send(self, content: bytes, blocking=True, callback=None):
        if not isinstance(content, bytes):
            raise RuntimeError(f'Expected bytes, got {type(content)}')

        with self.transaction_lock:
            self.wait_for_connect() # Wait for connect in case the channel got disconnected

            assert self.pending_transaction is None

            self.pending_transaction = OrderedChannel.Transaction(content, self.local_seq_number + 1, self.fragment_mtu(), None, None, callback)
            transaction = self.pending_transaction
            logger.info(f'Scheduling transaction on channel {self.id}. Fragments = [{self.pending_transaction.current_seq_number()} - {self.pending_transaction.last_seq_number()}]')

            self.interrupt_receive.set()

        if blocking:
            # Wait until the transaction is complete to return. Might throw if it timed out
            transaction.wait()

    def wait_for_connect(self, timeout=None):
        if not self.connected_event.wait(timeout):
            return False

        if self.connect_error:
            raise self.connect_error

        return True
