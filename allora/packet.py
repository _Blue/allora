import functools
import struct
from .errors import MalformedPacketException
from enum import Enum

try:
    cached_property = functools.cached_property
except AttributeError:
    cached_property = property


ORDERED_MAGIC = 0xafaf
ENDIAN = 'big'
MAX_TTL = 15


class Packet:
    class Flags(Enum):
        FRAGMENTED = 1
        REPLY = 2
        RELAYED = 4

    def __init__(self, content: bytes, payload_offset = 3):
        self.content = content
        self.payload_offset = payload_offset
        self.signal_strength = None

    def set_signal_strength(self, value: int):
        assert self.signal_strength is None
        self.signal_strength = value

    def read_int(self, offset: int, size: int) -> int:
        assert offset >= 0
        assert size > 0

        if offset + size > len(self.content):
            raise MalformedPacketException(f'Attempted to read at offset {offset + size}, but packet size is {len(self.content)}')

        return int.from_bytes(self.content[offset:offset + size], byteorder=ENDIAN)

    def read_float(self, offset: int, size: int) -> float:
        assert offset >= 0
        assert size > 0

        if offset + size > len(self.content):
            raise MalformedPacketException(f'Attempted to read at offset {offset + size}, but packet size is {len(self.content)}')

        return struct.unpack('d', self.content[offset:offset + size])[0]

    def read_payload_int(self, offset: int, size: int) -> int:
        return self.read_int(offset + self.payload_offset, size)

    def read_payload_float(self, offset: int, size: int) -> float:
        return self.read_float(offset + self.payload_offset, size)

    @property
    def source(self) -> int:
        return self.read_int(0, 1) & 0xf

    @property
    def destination(self) -> int:
        return (self.read_int(0, 1) & 0xf0) >> 4

    @property
    def channel(self) -> int:
        return self.read_int(1, 1)

    @property
    def flags(self) -> int:
        return self.read_int(2, 1) & 0xf

    @flags.setter
    def flags(self, flags: int):
        assert flags < 0xf

        return self.write_int(2, flags | (self.ttl << 4))

    @property
    def ttl(self) -> int:
        return (self.read_int(2, 1) & 0xf0) >> 4

    @ttl.setter
    def ttl(self, ttl: int):
        if ttl > MAX_TTL:
            raise MalformedPacketException(f'ttl {ttl} is over the maximum ({MAX_TTL})')

        self.write_int(2, self.flags | (ttl << 4))

    def write_int(self, index: int, value: int):
        array = bytearray(self.content)
        array[index] = value

        self.content = array

    @property
    def fragmented(self) -> bool:
        return (self.flags & Packet.Flags.FRAGMENTED.value) == Packet.Flags.FRAGMENTED.value

    @property
    def relayed(self) -> bool:
        return (self.flags & Packet.Flags.RELAYED.value) == Packet.Flags.RELAYED.value

    @property
    def payload(self) -> bytes:
        return self.content[self.payload_offset:]

    def __repr__(self) -> str:
        return f'Packet:\n source={self.source}\n destination={self.destination}\n channel={self.channel}\n flags={self.flags}\n ttl={self.ttl}\n payload={self.payload_repr()}'

    def payload_repr(self) -> str:
        return f'{len(self.payload)} bytes: {self.payload}'

    def raw_content(self) -> bytes:
        return self.content

    def __eq__(self, other) -> bool:
        return other is not None and self.raw_content() == other.raw_content()

    def eat_payload_bytes(self, count: int):
        if self.payload_offset + count > len(self.content):
            raise MalformedPacketException(f'Attempted to eat at offset {self.payload_offset + count}, but packet size is {len(self.content)}')

        self.payload_offset += count

class OrderedPacketType(Enum):
    CONTROL = 1
    DATA = 2
    ACK = 3

class OrderedPacket(Packet):
    def __init__(self, packet: Packet):
        super().__init__(packet.content, packet.payload_offset)
        self.validate()

    def packet_type(self) -> OrderedPacketType:
        raise RuntimeError('Pure call')

class AckPacket(OrderedPacket):

    def validate(self):
        size = len(super().payload)
        if size != 6:
            raise MalformedPacketException('Invalid length for ACK packet: {size}')

        if self.packet_type != OrderedPacketType.ACK:
            raise MalformedPacketException(f'Unexpected packet type in ack packet: {self.packet_type}')

    @property
    def seq_number(self) -> int:
        return self.read_payload_int(2, 4)

    @property
    def packet_type(self) -> OrderedPacketType:
        return OrderedPacketType.ACK

    def payload_repr(self) -> str:
        return f'AckPacket(seq_number={self.seq_number})'


class ConnectPacket(OrderedPacket):
    def validate(self):
        size = len(super().payload)
        if size != 16:
            raise MalformedPacketException(f'Unexpected connect packet size: {size}')

        if self.packet_type != OrderedPacketType.CONTROL:
            raise MalformedPacketException(f'Unexpected packet type in connect packet: {self.packet_type}')

        magic = super().read_payload_int(2, 2)
        if magic != ORDERED_MAGIC:
            raise MalformedPacketException(f'Unexpected ordered packet magic: {magic}')

    @property
    def connect_id(self) -> int:
        return super().read_payload_int(4, 4)

    @property
    def packet_type(self) -> OrderedPacketType:
        return OrderedPacketType.CONTROL

    @property
    def is_reply(self) -> bool:
        return (self.flags & Packet.Flags.REPLY.value) == Packet.Flags.REPLY.value

    @property
    def local_seq_number(self) -> int:
        return super().read_payload_int(8, 4)

    @property
    def remote_seq_number(self) -> int:
        return super().read_payload_int(12, 4)

    def payload_repr(self) -> str:
        return f'ConnectPacket(connect_id={self.connect_id},reply={self.is_reply},local_seq_number={self.local_seq_number},remote_seq_number={self.remote_seq_number})'


class DataPacket(OrderedPacket):

    def validate(self):
        size = len(super().payload)
        if size < 6:
            raise MalformedPacketException(f'Unexpected data packet size: {size}')

        if self.packet_type != OrderedPacketType.DATA:
            raise MalformedPacketException(f'Unexpected packet type in data packet: {self.packet_type}')

    @property
    def seq_number(self) -> int:
        return super().read_payload_int(2, 4)

    @property
    def packet_type(self) -> OrderedPacketType:
        return OrderedPacketType.DATA

    @property
    def payload(self) -> bytes:
        return self.content[self.payload_offset + 6:]

    def read_payload_int(self, offset: int, size: int) -> int:
        return super().read_payload_int(offset + 6, size)

    def read_payload_float(self, offset: int, size: int) -> float:
        return super().read_payload_float(offset + 6, size)

    def payload_repr(self) -> str:
        return f'DataPacket(seq_number={self.seq_number}, data={self.payload})'

class FragmentedDataPacket(DataPacket):
    def __init__(self, packet: Packet):
        super().__init__(packet)
        self.fragments = [packet]

    def validate(self):
        super().validate()
        if not self.fragmented:
            raise MalformedPacketException(f'FragmentedDataPacket needs the fragmented flag set')

    def is_complete(self) -> bool:
        return not self.fragments[-1].fragmented

    def add_fragment(self, fragment: DataPacket):
        assert not isinstance(fragment, FragmentedDataPacket)
        expected_seq_number = self.fragments[-1].seq_number + 1
        if fragment.seq_number != expected_seq_number:
            raise MalformedPacketException(f"Unexpected DataPacket fragment. Expected seq_number={expected_seq_number}, got seq_number={fragment.seq_number}")

        self.fragments.append(fragment)

    @cached_property
    def payload(self) -> bytes:
        if not self.is_complete():
            raise RuntimeError("Can't access payload of incomplete fragmented packet")

        assert len(self.fragments) > 1
        payload = bytes()

        for e in self.fragments:
            payload += e.payload

        return payload

    def read_payload_int(self, offset: int, size: int) -> int:
        if offset + size > len(self.payload):
            raise MalformedPacketException(f'Attempted to read at offset {offset + size}, but packet size is {len(self.payload)}')

        return int.from_bytes(self.payload[offset:offset + size], byteorder=ENDIAN)

    def raw_content(self) -> bytes:
        # Note: this method will only include the first fragment's header, for simplicity

        assert self.is_complete()

        content = super().raw_content()

        for e in self.fragments[1:]:
            content += e.payload

        return content

class Parser:
    def __init__(self, encryption):
        self.encryption = encryption

    def parse(self, packet: bytes):
        if len(packet) < 3:
            raise MalformedPacketException(f'Packet is too small {len(packet)}, expected at least 8')

        packet = Packet(self.encryption.decrypt(packet))

        return packet

    def parse_ordered(self, packet: Packet):
        packet_type = packet.read_payload_int(0, 2)

        if packet_type == OrderedPacketType.CONTROL.value:
            return ConnectPacket(packet)
        elif packet_type == OrderedPacketType.DATA.value:
            return DataPacket(packet)
        elif packet_type == OrderedPacketType.ACK.value:
            return AckPacket(packet)
        else:
            raise MalformedPacketException(f'Failed to parse ordered packet. Unexpected type: {packet_type}')

class Builder:
    def __init__(self, encryption):
        self.encryption = encryption

    def build(self, payload: bytes, source: int, destination: int, channel: int, flags: int, ttl: int) -> bytes:
        plaintext = ((source | (destination << 4)).to_bytes(1, ENDIAN) +
                    channel.to_bytes(1, ENDIAN) +
                    (flags | (ttl << 4)).to_bytes(1, ENDIAN) +
                    payload)

        return self.encryption.encrypt(plaintext)
