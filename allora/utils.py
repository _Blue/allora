from queue import Queue, Empty
from threading import Event
from datetime import datetime, timedelta
from time  import time, sleep

def sleep_until(end: float):
    while True:
        now = time()
        diff = end - now

        if diff <= 0:
            return
        else:
            sleep(diff / 2)

def run_until_timeout(predicate, timeout: float, pause_time: float) -> bool:
    deadline = time() + timeout
    while not predicate():

        now = time()
        if now > deadline:
            raise TimeoutError()

        sleep_until(now + pause_time)

def unique_list(input: list) -> list:
    output = []

    for e in input:
        if e not in output:
            output.append(e)

    return output

def interruptible_queue_get(queue: Queue, interrupt: Event, timeout_seconds: float, poll_rate_seconds=0.1):
    if timeout_seconds is not None:
        deadline = datetime.now() + timedelta(seconds=timeout_seconds)

    while not interrupt.is_set():
        try:
            return queue.get(timeout=poll_rate_seconds)
        except Empty:
            if timeout_seconds is not None and datetime.now() > deadline:
                raise

    raise Empty()
