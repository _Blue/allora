
class TransactionTimeout(RuntimeError):
    pass

class MalformedPacketException(RuntimeError):
    def __init__(self, error: str):
        super().__init__(error)

class UnauthenticatedPacketException(RuntimeError):
    def __init__(self, error: str):
        super().__init__(error)

class StoppedChannelException(RuntimeError):
    def __init__(self, error='Channel is stopped'):
        super().__init__(error)

class StoppedTransportException(RuntimeError):
    def __init__(self, error='Transport is not running'):
        super().__init__(error)

class DeviceError(RuntimeError):
    def __init__(self, error: str):
        super().__init__(error)

