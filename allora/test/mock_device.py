from allora.utils import interruptible_queue_get
from threading import Event
from queue import Queue, Empty

class MockDevice:

    def __init__(self):
        self.send_callback = None
        self.receive_queue = Queue(100)
        self.packet_received = False

    def initialize(self):
        pass

    def set_send_callback(self, callback):
        self.send_callback = callback

    def send(self, payload: bytes):
        assert self.send_callback is not None
        return self.send_callback(payload)

    def set_next_packet(self, next_packet: bytes):
        self.receive_queue.put(next_packet)

    def receive(self, timeout_seconds: float, interrupt_event: Event) -> bytes:
        # Signal the task done on the next receive() call after the actual receive 
        # to make sure that the packet has been added the the transport's receive queue
        if self.packet_received:
            self.receive_queue.task_done()


        self.packet_received = False

        try:
            packet = interruptible_queue_get(self.receive_queue, interrupt_event, timeout_seconds)
            assert packet is not None
            self.packet_received = True
            return packet, None
        except Empty:
            return None, None

    def join(self):
        self.receive_queue.join()

    @property
    def mtu(self):
        return 255

def get_connected_devices():

    left = MockDevice()
    right = MockDevice()

    left.set_send_callback(lambda e: right.set_next_packet(e))
    right.set_send_callback(lambda e: left.set_next_packet(e))

    return left, right
