import logging
import time
from allora.channel import OrderedChannel, BroadcastChannel
from allora.transport import Transport, BROADCAST_ADDRESS
from allora.encryption import EncryptionLayer
from .mock_device import get_connected_devices
from allora.packet import Packet, Builder, Parser, OrderedPacketType
from allora.errors import TransactionTimeout
from allora.utils import run_until_timeout
from queue import Queue, Empty
from threading import Thread
from datetime import timedelta, datetime

simulated = True

left = False

TEST_KEY = bytes([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16])

ENCRYPTION = EncryptionLayer(TEST_KEY)
PACKET_BUILDER = Builder(ENCRYPTION)
PACKET_PARSER = Parser(ENCRYPTION)

BROADCAST_TEST_PAYLOAD_1 = 'Broadcast1!'.encode()
BROADCAST_TEST_PAYLOAD_2 = 'Broadcast2!'.encode()
BROADCAST_TEST_PAYLOAD_3 = 'Broadcast3!'.encode()

BROADCAST_TEST_PAYLOAD_1_ACK = 'Broadcast1Ack'.encode()
BROADCAST_TEST_PAYLOAD_2_ACK = 'Broadcast2Ack'.encode()
BROADCAST_TEST_PAYLOAD_3_ACK = 'Broadcast3Ack'.encode()

ORDERED_TEST_PAYLOAD = 'Order1'.encode()
ORDERED_TEST_PAYLOAD_2 = 'Order2'.encode()
ORDERED_TEST_PAYLOAD_3 = 'Order3'.encode()
ORDERED_TEST_PAYLOAD_4 = 'Order4'.encode()
ORDERED_TEST_PAYLOAD_5 = 'Order5'.encode()
ORDERED_TEST_PAYLOAD_6 = 'Order6'.encode()

BIG_PAYLOAD = ('BigPayload' * 100).encode()
DONE_PAYLOAD = ('DonePayload' * 150).encode()
RESUME_TEST_PAYLOAD = 'ResumeTheTests'.encode()
RESUME_TEST_PAYLOAD_2 = 'ResumeTheTests2'.encode()
RESUME_TEST_PAYLOAD_3 = 'ResumeTheTests3'.encode()
RESUME_TEST_PAYLOAD_4 = 'ResumeTheTests4'.encode()

TEST_COMPLETE_PAYLOAD = 'TestComplete!'.encode()


MASTER_CHANNEL = 11
TEST_CHANNEL = 12
TEST_CHANNEL_2 = 13
TEST_CHANNEL_3 = 14
TEST_CHANNEL_4 = 15

def test_end_to_end():
    if simulated:
        left_device, right_device = get_connected_devices()

        if left:
            left_thread = Thread(target=left_test_entry, args=(left_device,))
            left_thread.start()

            right_test_entry(right_device)
            left_thread.join()
        else:
            right_thread = Thread(target=right_test_entry, args=(right_device,))
            right_thread.start()

            left_test_entry(left_device)
            right_thread.join()
    else:
        import busio
        import board
        from digitalio import DigitalInOut
        from allora.device import Rfm9x

        # Edit to match hardware configuration if needed
        CS = DigitalInOut(board.CE1)
        RESET = DigitalInOut(board.D25)
        spi = busio.SPI(board.SCK, MOSI=board.MOSI, MISO=board.MISO)
        device = Rfm9x(spi, CS, RESET, 5, 915.0)

        if left:
            left_test_entry(device)
        else:
            right_test_entry(device)


def make_packet(payload: bytes, source: int, destination: int, channel: int, flags=0):
    content = PACKET_BUILDER.build(payload, source, destination, channel, flags, 0)

    return PACKET_PARSER.parse(content)


def make_ordered_packet(payload: bytes, source: int, destination: int, channel: int, seq_number: int, flags=0):
    payload = OrderedPacketType.DATA.value.to_bytes(2, 'big') + seq_number.to_bytes(4, 'big') + payload
    content = PACKET_BUILDER.build(payload, source, destination, channel, flags, 0)

    return PACKET_PARSER.parse_ordered(PACKET_PARSER.parse(content))

def build_transport(device, address: int) -> Transport:
    encryption = EncryptionLayer(TEST_KEY)

    return Transport(device, encryption, address)


def clear_queue(queue: Queue):
    while not queue.empty():
        queue.get()


def broadcast_until_receive(channel: BroadcastChannel, payload: bytes, queue: Queue, expected_packet: Packet, timeout=10):
    def broadcast():
        channel.send(payload)
        if not queue.empty():

            while not queue.empty():
                assert queue.get() == expected_packet
            return True
        else:
            return False

    run_until_timeout(broadcast, timeout, 1.6)

def receive(queue: Queue, timeout=5):
    try:
        return queue.get(timeout=timeout)
    except Empty:
        return None

def expect_packet(queue: Queue, packet: Packet, timeout=10, strict=True):
    if strict:
        assert receive(queue, timeout) == packet
    else:
        deadline = datetime.now() + timedelta(seconds=timeout)
        while datetime.now() < deadline:
            if receive(queue, timeout) == packet:
                return



def left_test_entry(device):
    transport = build_transport(device, 1)
    receive_queue = Queue(100)
    master_queue = Queue(100)

    def on_receive(packet: Packet):
        receive_queue.put(packet)

    master_channel = OrderedChannel(transport, MASTER_CHANNEL, 2, 30, 2, 60)
    master_channel.set_receive_callback(lambda e: master_queue.put(e))

    channel_1 = BroadcastChannel(transport, TEST_CHANNEL)
    channel_1.set_receive_callback(on_receive)

    with transport, master_channel:
        with channel_1:
            broadcast_until_receive(channel_1, BROADCAST_TEST_PAYLOAD_1, master_queue, make_ordered_packet(BROADCAST_TEST_PAYLOAD_1_ACK, 2, 1, MASTER_CHANNEL, 1, 0))
            broadcast_until_receive(channel_1, BROADCAST_TEST_PAYLOAD_2, master_queue, make_ordered_packet(BROADCAST_TEST_PAYLOAD_2_ACK, 2, 1, MASTER_CHANNEL, 2, 0))

            expect_packet(receive_queue, make_packet(BROADCAST_TEST_PAYLOAD_3, 2, BROADCAST_ADDRESS, TEST_CHANNEL, 0), 30, strict=True)
            master_channel.send(BROADCAST_TEST_PAYLOAD_3_ACK)

        clear_queue(receive_queue)
        channel_2 = OrderedChannel(transport, TEST_CHANNEL_2, 2, 10, 1, 10)
        channel_2.set_receive_callback(on_receive)
        with channel_2:
            channel_2.wait_for_connect()

            channel_2.send(ORDERED_TEST_PAYLOAD)
            channel_2.send(ORDERED_TEST_PAYLOAD_2)
            channel_2.send(ORDERED_TEST_PAYLOAD_3)
            expect_packet(receive_queue, make_ordered_packet(ORDERED_TEST_PAYLOAD_4, 2, 1, TEST_CHANNEL_2, 1, 0))
            expect_packet(master_queue, make_ordered_packet(RESUME_TEST_PAYLOAD, 2, 1, MASTER_CHANNEL, 3, 0))


        # Open channel 3 with no one listening and expect a timeout:
        channel_3 = OrderedChannel(transport, TEST_CHANNEL_3, 2, 3, 1, 5)
        with channel_3:
            error = None
            try:
                channel_3.wait_for_connect()
            except Exception as e:
                error = e

            assert isinstance(error, TimeoutError)


        # Start an exchange on channel 4, then have it time out
        channel_4 = OrderedChannel(transport, TEST_CHANNEL_4, 2, 100, 1, 10)
        channel_4.set_receive_callback(on_receive)
        with channel_4:
            channel_4.wait_for_connect()
            channel_4.send(ORDERED_TEST_PAYLOAD_5) # LEFT HERE <=======
            master_channel.send(RESUME_TEST_PAYLOAD_2)
            expect_packet(master_queue, make_ordered_packet(RESUME_TEST_PAYLOAD_3, 2, 1, MASTER_CHANNEL, 4, 0))

            try:
                error = None
                channel_4.send('Dummy'.encode())
            except Exception as e:
                error = e

            assert isinstance(error, TransactionTimeout)

            # Then send a broadcast master to tell the other end to restart the channel
            master_channel.send(RESUME_TEST_PAYLOAD_4)

            channel_4.send(ORDERED_TEST_PAYLOAD_6) # LEFT IS HERE
            expect_packet(receive_queue, make_ordered_packet(BIG_PAYLOAD, 2, 1, TEST_CHANNEL_4, 1, Packet.Flags.FRAGMENTED.value), 30)
            channel_4.send(BIG_PAYLOAD)
            expect_packet(receive_queue, make_ordered_packet(DONE_PAYLOAD, 2, 1, TEST_CHANNEL_4, 6, Packet.Flags.FRAGMENTED.value), 30)

        try:
            master_channel.send(TEST_COMPLETE_PAYLOAD)
        except TransactionTimeout:
            pass # Might timeout since the other side might be disconnected, so ACK packet might be lost

        logging.info(f'Stats for channel_2: {channel_2.statistics()}')
        logging.info(f'Stats for channel_3: {channel_2.statistics()}')
        logging.info(f'Stats for channel_4: {channel_4.statistics()}')
        logging.info(f'Stats for master channel: {master_channel.statistics()}')


def right_test_entry(device):
    transport = build_transport(device, 2)
    receive_queue = Queue(100)
    master_queue = Queue(100)

    def on_receive(packet: Packet):
        receive_queue.put(packet)

    channel_1 = BroadcastChannel(transport, TEST_CHANNEL)
    channel_1.set_receive_callback(on_receive)

    master_channel = OrderedChannel(transport, MASTER_CHANNEL, 1, 30, 2, 60)
    master_channel.set_receive_callback(lambda e: master_queue.put(e))

    with transport, master_channel:
        with channel_1:
            expect_packet(receive_queue, make_packet(BROADCAST_TEST_PAYLOAD_1, 1, BROADCAST_ADDRESS, TEST_CHANNEL, 0), 30, strict=True)
            master_channel.send(BROADCAST_TEST_PAYLOAD_1_ACK)

            expect_packet(receive_queue, make_packet(BROADCAST_TEST_PAYLOAD_2, 1, BROADCAST_ADDRESS, TEST_CHANNEL, 0), 30, strict=False)
            master_channel.send(BROADCAST_TEST_PAYLOAD_2_ACK)

            broadcast_until_receive(channel_1, BROADCAST_TEST_PAYLOAD_3, master_queue, make_ordered_packet(BROADCAST_TEST_PAYLOAD_3_ACK, 1, 2, MASTER_CHANNEL, 1, 0))
            channel_1.send(BROADCAST_TEST_PAYLOAD_3)


        clear_queue(receive_queue)
        channel_2 = OrderedChannel(transport, TEST_CHANNEL_2, 1, 10, 1, 5)
        channel_2.set_receive_callback(on_receive)
        with channel_2:
            channel_2.wait_for_connect()

            expect_packet(receive_queue, make_ordered_packet(ORDERED_TEST_PAYLOAD, 1, 2, TEST_CHANNEL_2, 1, 0))
            expect_packet(receive_queue, make_ordered_packet(ORDERED_TEST_PAYLOAD_2, 1, 2, TEST_CHANNEL_2, 2, 0))
            expect_packet(receive_queue, make_ordered_packet(ORDERED_TEST_PAYLOAD_3, 1, 2, TEST_CHANNEL_2, 3, 0))
            channel_2.send(ORDERED_TEST_PAYLOAD_4)
            master_channel.send(RESUME_TEST_PAYLOAD)


        channel_4 = OrderedChannel(transport, TEST_CHANNEL_4, 1, 100, 1, 10)
        channel_4.set_receive_callback(on_receive)
        with channel_4:
            channel_4.wait_for_connect()
            expect_packet(receive_queue, make_ordered_packet(ORDERED_TEST_PAYLOAD_5, 1, 2, TEST_CHANNEL_4, 1, 0), 30)
            expect_packet(master_queue, make_ordered_packet(RESUME_TEST_PAYLOAD_2, 1, 2, MASTER_CHANNEL, 2, 0))

        # Send a packet after channel_4 is closed
        master_channel.send(RESUME_TEST_PAYLOAD_3)

        # Then wait for a master packet to restart the channel:
        expect_packet(master_queue, make_ordered_packet(RESUME_TEST_PAYLOAD_4, 1, 2, MASTER_CHANNEL, 3, 0), 60)

        with channel_4:
            channel_4.wait_for_connect()
            expect_packet(receive_queue, make_ordered_packet(ORDERED_TEST_PAYLOAD_6, 1, 2, TEST_CHANNEL_4, 2, 0), 30)

            channel_4.send(BIG_PAYLOAD)
            expect_packet(receive_queue, make_ordered_packet(BIG_PAYLOAD, 1, 2, TEST_CHANNEL_4, 3, Packet.Flags.FRAGMENTED.value), 30)
            channel_4.send(DONE_PAYLOAD)



        expect_packet(master_queue, make_ordered_packet(TEST_COMPLETE_PAYLOAD, 1, 2, MASTER_CHANNEL, 4, 0), 60)
        time.sleep(5) # Give 5 seconds for other end to receive the final ack


        logging.info(f'Stats for channel_2: {channel_2.statistics()}')
        logging.info(f'Stats for channel_4: {channel_4.statistics()}')
        logging.info(f'Stats for master channel: {master_channel.statistics()}')
