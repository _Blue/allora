import random
from .mock_device import *
from allora.transport import Transport, BROADCAST_ADDRESS
from allora.encryption import NullEncryption
from allora.packet import *
from allora.channel import BroadcastChannel, OrderedChannel
from .test_transport import MockTransport, make_payload
from allora.errors import TransactionTimeout
from allora.utils import unique_list
from threading import Event

class MockBroadcastChannel(BroadcastChannel):
    def __init__(self, channel_id: int, address=1):
        super().__init__(MockTransport(address, disable_receive_callback=True), channel_id)
        self.expected_receives = []

        self.set_receive_callback(self.on_packet_received)

    def on_packet_received(self, packet: Packet):
        assert len(self.expected_receives) > 0
        expected_packet = self.expected_receives[0]

        assert packet == expected_packet
        del self.expected_receives[0]

        return True

    def expect_receive(self, packet: Packet):
        self.expected_receives.append(packet)

    def validate(self):
        if self.running:
            self.sync()

        self.transport.validate()
        assert len(self.expected_receives) == 0

    def __del__(self):
        self.validate()

    def __exit__(self, *args, **kwargs):
        self.sync()
        super().__exit__(*args, **kwargs)

    def sync(self):
        self.transport.sync()
        super().sync()

class MockOrderedChannel(OrderedChannel):
    def __init__(self, channel_id: int, transport = None, address=1, destination=2, connect_timeout=10, ack_timeout=0.5, disconnect_timeout=10):
        if not transport:
            transport = MockTransport(address, disable_receive_callback=True)

        super().__init__(transport, channel_id, destination, connect_timeout, ack_timeout, disconnect_timeout)
        self.expected_receives = []

        self.set_receive_callback(self.on_packet_received)
        self.expected_states = []

    def on_packet_received(self, packet: Packet):
        assert len(self.expected_receives) > 0
        expected_packet = self.expected_receives[0]

        assert packet == expected_packet
        del self.expected_receives[0]

        return True

    def expect_receive(self, packet: Packet):
        self.expected_receives.append(packet)

    def validate(self):
        if self.running:
            self.sync()

        self.transport.validate()
        assert len(self.expected_receives) == 0
        assert len(self.expected_states) == 0

    def expect_transition(self, state: OrderedChannel.State):
        self.expected_states.append(state)

    def transition(self, state, *args, **kwargs):
        assert state == self.expected_states[0]
        del self.expected_states[0]

        super().transition(state, *args, **kwargs)

    def __del__(self):
        self.validate()

    def start(self):
        return super().start(lambda e: None)

    def __enter__(self):
        if self.running:
            return self
        else:
            return super().__enter__()

    def __exit__(self, *args, **kwargs):
        self.sync()
        super().__exit__(*args, **kwargs)

    def sync(self):
        self.transport.sync()
        super().sync()

    def parse(self, content: bytes) -> OrderedPacket:
        return self.transport.parser.parse_ordered(self.transport.parser.parse(content))


def assert_throws(routine, exception: type):
    error = None

    try:
        routine()
    except Exception as e:
        error = e

    assert type(error) == exception

def get_connected_channel(send_callback, *args, **kwargs):
    connected = False

    def on_send(payload: bytes):
        packet = channel.transport.parser.parse(payload)

        if not connected:
            connect_id = ConnectPacket(packet).connect_id
            channel.transport.device.set_next_packet(make_payload(make_connect_payload(connect_id), 2, 1, 1, Packet.Flags.REPLY.value))
        else:
            send_callback(packet)

    transport = MockTransport(1, disable_receive_callback=True, send_callback=on_send)
    channel = MockOrderedChannel(1, transport=transport, *args, **kwargs)
    channel.expect_transition(OrderedChannel.State.CONNECTING)
    channel.expect_transition(OrderedChannel.State.CONNECTED)

    channel.transport.start()
    channel.start()

    channel.wait_for_connect()
    channel.sync()
    connected = True

    return channel, channel.transport

def test_basic_broadcast_channel():
    payload_1 = 'payload_1'.encode()
    payload_2 = 'payload_2'.encode()
    payload_3 = 'payload_3'.encode()

    channel = MockBroadcastChannel(1)
    with channel.transport, channel:

        # Receive a packet
        channel.expect_receive(Packet(make_payload(payload_1, 2, BROADCAST_ADDRESS, 1, 0)))
        channel.transport.device.set_next_packet(make_payload(payload_1, 2, BROADCAST_ADDRESS, 1, 0))
        channel.validate()

        # Send a packet
        channel.transport.expect_sent(make_payload(payload_2, 1, BROADCAST_ADDRESS, 1, 0))
        channel.send(payload_2)
        channel.validate()

        # Receive another packet
        channel.expect_receive(Packet(make_payload(payload_3, 2, BROADCAST_ADDRESS, 1, 0)))
        channel.transport.device.set_next_packet(make_payload(payload_3, 2, BROADCAST_ADDRESS, 1, 0))
        channel.validate()

def test_other_channels_are_dropped():
    payload_1 = 'payload_1'.encode()

    channel = MockBroadcastChannel(1)
    with channel.transport, channel:

        # Receive a packet
        channel.expect_receive(Packet(make_payload(payload_1, 2, BROADCAST_ADDRESS, 1, 0)))
        channel.transport.device.set_next_packet(make_payload(payload_1, 2, BROADCAST_ADDRESS, 1, 0))

        # Receive another packet on a different channel, shouldn't be passed up
        channel.transport.device.set_next_packet(make_payload(payload_1, 2, BROADCAST_ADDRESS, 2, 0))

        channel.validate()


def make_connect_payload(connection_id: int, local_seq_number=0, remote_seq_number=0) -> bytes:
    return (OrderedPacketType.CONTROL.value.to_bytes(2, 'big')
            + ORDERED_MAGIC.to_bytes(2, 'big')
            + connection_id.to_bytes(4, 'big')
            + local_seq_number.to_bytes(4, 'big')
            + remote_seq_number.to_bytes(4, 'big'))

def make_ack_payload(seq_id: int) -> bytes:
    return OrderedPacketType.ACK.value.to_bytes(2, 'big') + seq_id.to_bytes(4, 'big')

def make_data_payload(payload: str, seq_number: int) -> bytes:
    return make_binary_data_payload(payload.encode(), seq_number)

def make_binary_data_payload(payload: bytes, seq_number: int) -> bytes:
    return OrderedPacketType.DATA.value.to_bytes(2, 'big') + seq_number.to_bytes(4, 'big') + payload

def test_ordered_channel_connection_inf():
    connect_payload = make_connect_payload(2 ** 32 - 1)

    received_connect_ids = []
    def verify_connect_packet(payload: bytes):
        nonlocal received_connect_ids
        packet = channel.parse(payload)
        assert isinstance(packet, ConnectPacket)
        connect_id = packet.connect_id

        assert packet.source == 1
        assert packet.destination == 2
        assert packet.channel == 1

        if len(received_connect_ids) == 0:
            channel.transport.device.set_next_packet(make_payload(connect_payload, 2, 1, 1, 0))
            channel.expect_transition(OrderedChannel.State.DISCONNECTED)

        received_connect_ids.append(connect_id)

    transport = MockTransport(1, disable_receive_callback=True, send_callback=verify_connect_packet)
    channel = MockOrderedChannel(1, transport=transport)
    channel.expect_transition(OrderedChannel.State.CONNECTING)
    channel.expect_transition(OrderedChannel.State.CONNECTED)


    with channel.transport, channel:
        channel.transport.device.set_next_packet(make_payload(connect_payload, 2, 1, 1, 0))
        channel.wait_for_connect()

    assert len(received_connect_ids) >= 2 # The first connect packet might have been repeated
    for i in range(1, len(received_connect_ids) - 1):
        assert received_connect_ids[i - 1] == received_connect_ids[i]

    assert received_connect_ids[-1] == 2 ** 32 -1
    assert channel.connection_id == 2 ** 32 -1


def test_ordered_channel_connection_sup():
    received_connect_ids = []
    def verify_connect_packet(payload: bytes):
        nonlocal received_connect_ids
        packet = channel.parse(payload)
        assert isinstance(packet, ConnectPacket)
        connect_id = packet.connect_id


        assert packet.source == 1
        assert packet.destination == 2
        assert packet.channel == 1

        # If it's the first packet, send another with the remote connection id
        if len(received_connect_ids) == 0:
            channel.transport.device.set_next_packet(make_payload(make_connect_payload(connect_id), 2, 1, 1, 0))
            channel.expect_transition(OrderedChannel.State.DISCONNECTED)

        received_connect_ids.append(connect_id)

    transport = MockTransport(1, disable_receive_callback=True, send_callback=verify_connect_packet)
    channel = MockOrderedChannel(1, transport=transport)
    channel.expect_transition(OrderedChannel.State.CONNECTING)
    channel.expect_transition(OrderedChannel.State.CONNECTED)

    with channel.transport, channel:
        # Send the first packet with a low connection id
        channel.transport.device.set_next_packet(make_payload(make_connect_payload(0), 2, 1, 1, 0))
        channel.wait_for_connect()

    assert len(received_connect_ids) >= 1 # The first connect packet might have been repeated

    # Don't check the actual connection id because the zero-id might have caused a connection id reset

def test_ordered_channel_simple_send_data_none_connect_timeout():
    payload = '0xcafe'.encode()

    sent = []
    def on_send(packet: Packet):
        sent.append(packet)

        # Acknowledge packet
        channel.transport.device.set_next_packet(make_payload(make_ack_payload(1), 2, 1, 1, 0))

    channel, transport = get_connected_channel(on_send, connect_timeout=None)
    with transport, channel:
        channel.expect_transition(OrderedChannel.State.DISCONNECTED)
        channel.send(payload)

    assert len(sent) > 0
    for packet in sent:
        assert packet.read_payload_int(0, 2) == OrderedPacketType.DATA.value
        assert packet.read_payload_int(2, 4) == 1

        packet.eat_payload_bytes(6)

        assert packet.payload == payload


def test_ordered_channel_simple_send_data():
    payload = '0xcafe'.encode()

    sent = []
    def on_send(packet: Packet):
        sent.append(packet)

        # Acknowledge packet
        channel.transport.device.set_next_packet(make_payload(make_ack_payload(1), 2, 1, 1, 0))

    channel, transport = get_connected_channel(on_send)
    with transport, channel:
        channel.expect_transition(OrderedChannel.State.DISCONNECTED)
        channel.send(payload)

    assert len(sent) > 0
    for packet in sent:
        assert packet.read_payload_int(0, 2) == OrderedPacketType.DATA.value
        assert packet.read_payload_int(2, 4) == 1

        packet.eat_payload_bytes(6)

        assert packet.payload == payload

def test_ordered_channel_non_reply_connects_are_replied_to():
    sent = []
    def on_send(packet: Packet):
        sent.append(channel.transport.parser.parse_ordered(packet))

    channel, transport = get_connected_channel(on_send)
    with transport, channel:
        channel.transport.device.set_next_packet(make_payload(make_connect_payload(channel.connection_id), 2, 1, 1, 0))
        channel.expect_transition(OrderedChannel.State.DISCONNECTED)


    assert len(sent) == 1
    assert isinstance(sent[0], ConnectPacket)
    assert sent[0].connect_id == channel.connection_id
    assert sent[0].is_reply

def test_ordered_channel_reply_connects_are_not_replied_to():
    sent = []
    def on_send(packet: Packet):
        sent.append(channel.transport.parser.parse_ordered(packet))

    channel, transport = get_connected_channel(on_send)
    with transport, channel:
        channel.transport.device.set_next_packet(make_payload(make_connect_payload(channel.connection_id), 2, 1, 1, Packet.Flags.REPLY.value))
        channel.expect_transition(OrderedChannel.State.DISCONNECTED)

    assert len(sent) == 0


def test_ordered_channel_async_transaction():
    sent = []

    callback_called_event = Event()
    callback_return_event = Event()
    def on_send(packet: Packet):
        callback_return_event.wait()

        sent.append(packet)
        channel.transport.device.set_next_packet(make_payload(make_ack_payload(1), 2, 1, 1, 0))

    called = False
    def callback(error):
        nonlocal called
        assert not called
        called = True

        callback_called_event.set()

    channel, transport = get_connected_channel(on_send)
    with transport, channel:
        channel.expect_transition(OrderedChannel.State.DISCONNECTED)
        channel.send('payload'.encode(), blocking=False, callback=callback)
        callback_return_event.set()
        callback_called_event.wait()

    assert called

    assert len(sent) >= 1

def test_ordered_channel_async_transaction_timeout():
    sent = []

    callback_called_event = Event()
    def on_send(packet: Packet):
        sent.append(packet)

    error = None
    def callback(e):
        nonlocal error
        assert not error
        error = e
        callback_called_event.set()

    channel, transport = get_connected_channel(on_send, disconnect_timeout=2)
    with transport, channel:
        channel.expect_transition(OrderedChannel.State.CONNECTING)
        channel.expect_transition(OrderedChannel.State.DISCONNECTED)
        channel.send('payload'.encode(), blocking=False, callback=callback)
        callback_called_event.wait()

    assert error is not None
    assert len(sent) >= 1
    assert isinstance(error, TransactionTimeout)


def test_ordered_channel_simple_repeat_then_ack():
    payload = '0xcafe'.encode()

    sent = []
    def on_send(packet: Packet):
        sent.append(packet)

        # Acknowledge packet only after second retry

        if len(sent) == 2:
            channel.transport.device.set_next_packet(make_payload(make_ack_payload(1), 2, 1, 1, 0))

    channel, transport = get_connected_channel(on_send)
    with transport, channel:
        channel.expect_transition(OrderedChannel.State.DISCONNECTED)
        channel.send(payload)

    assert len(sent) >= 2
    for packet in sent:
        assert packet.read_payload_int(0, 2) == OrderedPacketType.DATA.value
        assert packet.read_payload_int(2, 4) == 1

        packet.eat_payload_bytes(6)

        assert packet.payload == payload

def test_ordered_channel_simple_repeat_twice_then_ack():
    payload = '0xcafe'.encode()

    sent = []
    def on_send(packet: Packet):
        sent.append(packet)

        # Acknowledge packet only after second retry

        if len(sent) == 3:
            channel.transport.device.set_next_packet(make_payload(make_ack_payload(1), 2, 1, 1, 0))

    channel, transport = get_connected_channel(on_send)
    with transport, channel:
        channel.expect_transition(OrderedChannel.State.DISCONNECTED)
        channel.send(payload)

    assert len(sent) >= 3
    for packet in sent:
        assert packet.read_payload_int(0, 2) == OrderedPacketType.DATA.value
        assert packet.read_payload_int(2, 4) == 1

        packet.eat_payload_bytes(6)

        assert packet.payload == payload

def test_ordered_channel_transaction_timeout():
    payload = '0xcafe'.encode()

    sent = []
    def on_send(packet: Packet):
        sent.append(packet)

    channel, transport = get_connected_channel(on_send, disconnect_timeout=2)
    error = None
    with transport, channel:
        channel.expect_transition(OrderedChannel.State.CONNECTING)
        channel.expect_transition(OrderedChannel.State.DISCONNECTED)
        try:
            channel.send(payload)
        except TransactionTimeout as e:
            error = e

    assert isinstance(error, TransactionTimeout)

    data_packets = [e for e in sent if e.read_payload_int(0, 2) == OrderedPacketType.DATA.value]
    assert len(data_packets) > 1
    for packet in data_packets:
        assert packet.read_payload_int(0, 2) == OrderedPacketType.DATA.value
        assert packet.read_payload_int(2, 4) == 1

        packet.eat_payload_bytes(6)

        assert packet.payload == payload

def test_ordered_channel_multiple_sends():
    sent = []
    def on_send(packet: Packet):
        sent.append(packet)

        # Acknowledge packet only after second retry

        if len(sent) == 3:
            channel.transport.device.set_next_packet(make_payload(make_ack_payload(1), 2, 1, 1, 0))

    channel, transport = get_connected_channel(on_send)
    with transport, channel:
        channel.expect_transition(OrderedChannel.State.DISCONNECTED)
        channel.send('payload1'.encode())

    assert len(sent) >= 3
    for packet in sent:
        assert packet.read_payload_int(0, 2) == OrderedPacketType.DATA.value
        assert packet.read_payload_int(2, 4) == 1

        packet.eat_payload_bytes(6)

        assert packet.payload.decode() == 'payload1'


def test_ordered_channel_send_multiple_packets():
    sent = []
    def on_send(packet: Packet):
        parsed = channel.transport.parser.parse_ordered(packet)
        sent.append(parsed)

        channel.transport.device.set_next_packet(make_payload(make_ack_payload(parsed.seq_number), 2, 1, 1, 0))

    channel, transport = get_connected_channel(on_send)
    with transport, channel:
        channel.expect_transition(OrderedChannel.State.DISCONNECTED)
        channel.send('payload1'.encode())
        channel.send('payload2'.encode())
        channel.send('payload3'.encode())

    assert len(sent) >= 3
    for packet in sent:
        assert isinstance(packet, DataPacket)

    seq_numbers = [e.seq_number for e in sent]
    assert 1 in seq_numbers
    assert 2 in seq_numbers
    assert 3 in seq_numbers

    payloads = [e.payload.decode() for e in sent]
    assert 'payload1' in payloads
    assert 'payload2' in payloads
    assert 'payload3' in payloads

def test_send_then_disconnect_then_resend():
    sent = []
    completed_connect = False
    data_received = False
    def on_send(packet: Packet):
        nonlocal completed_connect, data_received
        sent.append(packet)

        if len(sent) == 1:
            # Re-initiate a connection once message is received. Don't acknoledge it
            channel.expect_transition(OrderedChannel.State.CONNECTING)
            channel.transport.device.set_next_packet(make_payload(make_connect_payload(channel.connection_id - 1), 2, 1, 1, 0))
        else:
            packet_type = packet.read_payload_int(0, 2)

            if packet_type == OrderedPacketType.CONTROL.value:

                parsed = channel.transport.parser.parse_ordered(packet)
                connect_id = parsed.connect_id

                if not completed_connect:
                    channel.expect_transition(OrderedChannel.State.CONNECTED)
                completed_connect = True
                channel.transport.device.set_next_packet(make_payload(make_connect_payload(connect_id), 2, 1, 1, 0))

            elif packet_type == OrderedPacketType.DATA.value and completed_connect:
                packet.eat_payload_bytes(6)
                assert packet.payload.decode() == 'payload1'
                if not data_received:
                    channel.expect_transition(OrderedChannel.State.DISCONNECTED)
                    channel.transport.device.set_next_packet(make_payload(make_ack_payload(1), 2, 1, 1, 0))


                data_received = True

    channel, transport = get_connected_channel(on_send)
    with transport, channel:
        channel.send('payload1'.encode())

    assert len(sent) >= 3
    assert data_received
    assert completed_connect

def test_send_then_disconnect_then_resend_same_connection_id():
    sent = []

    completed_connect = False
    data_received = False
    received_ack = Event()
    def on_send(packet: Packet):
        nonlocal completed_connect, data_received, connection_id, received_ack

        packet = transport.parser.parse_ordered(packet)
        sent.append(packet)

        if isinstance(packet, DataPacket):
            if packet.seq_number == 1:
                assert not data_received
                # ack fist packet, and expect a receive
                channel.transport.device.set_next_packet(make_payload(make_ack_payload(1), 2, 1, 1, 0))
                channel.expect_receive(Packet(make_payload(make_data_payload('payload2', 1), 2, 1, 1, 0)))
                transport.device.set_next_packet(make_payload(make_data_payload('payload2', 1), 2, 1, 1, 0))
                data_received = True
            elif packet.seq_number == 2:
                # Let the first attempt time out, send a connect packet instead
                if not completed_connect:
                    channel.transport.device.set_next_packet(make_payload(make_connect_payload(connection_id), 2, 1, 1, 0))
                else:
                    transport.device.set_next_packet(make_payload(make_ack_payload(2), 2, 1, 1, 0))

        elif isinstance(packet, AckPacket):
            if packet.seq_number == 1:
                # Previous data packet has been acked, trigger a reconnect and send it again
                received_ack.set()

        elif isinstance(packet, ConnectPacket):
            # Reuse the same connect id here
            assert packet.connect_id == connection_id
            channel.transport.device.set_next_packet(make_payload(make_connect_payload(connection_id), 2, 1, 1, Packet.Flags.REPLY.value))
            completed_connect = True

    channel, transport = get_connected_channel(on_send, disconnect_timeout=3)
    connection_id = channel.connection_id
    with transport, channel:
        channel.send('payload1'.encode())
        received_ack.wait()
        channel.send('payload2'.encode())
        channel.expect_transition(OrderedChannel.State.DISCONNECTED)

    assert data_received
    assert completed_connect
    assert received_ack.is_set()


    # Connection id should be unchanged
    assert channel.connection_id == connection_id
    assert channel.local_seq_number == 2
    assert channel.remote_seq_number == 1


def test_basic_receive():
    sent = []

    def on_send(packet: Packet):
        sent.append(packet)

    channel, transport = get_connected_channel(on_send)
    with transport, channel:
        channel.expect_receive(Packet(make_payload(make_data_payload('payload', 1), 2, 1, 1, 0)))
        transport.device.set_next_packet(make_payload(make_data_payload('payload', 1), 2, 1, 1, 0))

        channel.expect_transition(OrderedChannel.State.DISCONNECTED)
        transport.device.join()


    assert len(sent) >= 1
    assert sent[0].read_payload_int(0, 2) == OrderedPacketType.ACK.value
    assert sent[0].read_payload_int(2, 4) == 1

def test_ordered_channel_data_packet_received_twice():
    sent = []

    def on_send(packet: Packet):
        sent.append(packet)

    channel, transport = get_connected_channel(on_send)
    with transport, channel:
        channel.expect_receive(Packet(make_payload(make_data_payload('payload', 1), 2, 1, 1, 0)))
        transport.device.set_next_packet(make_payload(make_data_payload('payload', 1), 2, 1, 1, 0))

        # Repeat the data packet with the same seq number to make sure it's not indicated twice
        transport.device.set_next_packet(make_payload(make_data_payload('payload', 1), 2, 1, 1, 0))

        channel.expect_transition(OrderedChannel.State.DISCONNECTED)
        transport.device.join()


    assert len(sent) >= 1
    assert sent[0].read_payload_int(0, 2) == OrderedPacketType.ACK.value
    assert sent[0].read_payload_int(2, 4) == 1

def test_multiple_receives():
    sent = []

    def on_send(packet: Packet):
        sent.append(transport.parser.parse_ordered(packet))

    channel, transport = get_connected_channel(on_send)
    with transport, channel:
        channel.expect_receive(Packet(make_payload(make_data_payload('payload', 1), 2, 1, 1, 0)))
        channel.expect_receive(Packet(make_payload(make_data_payload('payload2', 2), 2, 1, 1, 0)))
        channel.expect_receive(Packet(make_payload(make_data_payload('payload3', 3), 2, 1, 1, 0)))
        transport.device.set_next_packet(make_payload(make_data_payload('payload', 1), 2, 1, 1, 0))
        transport.device.set_next_packet(make_payload(make_data_payload('payload2', 2), 2, 1, 1, 0))
        transport.device.set_next_packet(make_payload(make_data_payload('payload3', 3), 2, 1, 1, 0))

        channel.expect_transition(OrderedChannel.State.DISCONNECTED)
        transport.device.join()


    assert len(sent) >= 3
    for e in sent:
        assert isinstance(e, AckPacket)

    assert set([e.seq_number for e in sent]) == set([1, 2, 3])


def test_receive_then_disconnect_then_receive():
    sent = []

    connect_done = False
    def on_send(packet: Packet):
        nonlocal connect_done
        sent.append(transport.parser.parse_ordered(packet))

        if isinstance(sent[-1], ConnectPacket) and not connect_done:
            channel.expect_transition(OrderedChannel.State.CONNECTED)
            channel.expect_transition(OrderedChannel.State.DISCONNECTED)
            transport.device.set_next_packet(make_payload(make_connect_payload(sent[-1].connect_id), 2, 1, 1, 0))

            channel.expect_receive(Packet(make_payload(make_data_payload('payload2', 1), 2, 1, 1, 0)))
            transport.device.set_next_packet(make_payload(make_data_payload('payload2', 1), 2, 1, 1, 0))
            connect_done = True


    channel, transport = get_connected_channel(on_send)
    with transport, channel:
        channel.expect_receive(Packet(make_payload(make_data_payload('payload', 1), 2, 1, 1, 0)))
        transport.device.set_next_packet(make_payload(make_data_payload('payload', 1), 2, 1, 1, 0))

        transport.sync()

        # Trigger reconnect
        channel.expect_transition(OrderedChannel.State.CONNECTING)
        channel.transport.device.set_next_packet(make_payload(make_connect_payload(0), 2, 1, 1, 0))


    acks = [e.seq_number for e in sent if isinstance(e, AckPacket)]
    assert acks == [1, 1] # Because the disconnect resets seq numbers

def test_receive_then_disconnect_then_receive_ack_lost():
    sent = []

    connect_done = False
    def on_send(packet: Packet):
        nonlocal connect_done

        packet = transport.parser.parse_ordered(packet)
        sent.append(packet)

        if isinstance(packet, DataPacket):
            if not connect_done:
                # Purpusefully don't send an ack, let the channel time out
                return
            else: # seq_number should 2 or 3
                transport.device.set_next_packet(make_payload(make_ack_payload(packet.seq_number), 2, 1, 1, 0))

        if isinstance(packet, ConnectPacket) and not connect_done:
            channel.expect_transition(OrderedChannel.State.CONNECTED)
            transport.device.set_next_packet(make_payload(make_connect_payload(packet.connect_id, 0, 1), 2, 1, 1, Packet.Flags.REPLY.value))

            connect_done = True

    channel, transport = get_connected_channel(on_send, disconnect_timeout=2)
    with transport, channel:

        channel.expect_transition(OrderedChannel.State.CONNECTING)
        # Create a transaction that times out
        assert_throws(lambda: channel.send('Payload1'.encode()), TransactionTimeout)

        # Then send another and make sure that the seq_number is correct
        channel.send('Payload2'.encode())
        channel.send('Payload3'.encode())
        channel.expect_transition(OrderedChannel.State.DISCONNECTED)
        assert channel.local_seq_number == 3

    data_packets = unique_list([e for e in sent if isinstance(e, DataPacket)])
    assert data_packets[0].seq_number == 1
    assert data_packets[1].seq_number == 2
    assert data_packets[2].seq_number == 3

def test_receive_then_disconnect_then_receive_ack_lost_reset_channel():
    sent = []

    connect_done = False
    def on_send(packet: Packet):
        nonlocal connect_done

        packet = transport.parser.parse_ordered(packet)
        sent.append(packet)

        if isinstance(packet, DataPacket):
            if not connect_done:
                # Purpusefully don't send an ack, let the channel time out
                return
            else: # seq_number should 1 or 1
                transport.device.set_next_packet(make_payload(make_ack_payload(packet.seq_number), 2, 1, 1, 0))

        if isinstance(packet, ConnectPacket) and not connect_done:
            channel.expect_transition(OrderedChannel.State.CONNECTED)
            transport.device.set_next_packet(make_payload(make_connect_payload(packet.connect_id, 0, 0), 2, 1, 1, Packet.Flags.REPLY.value))

            connect_done = True

    channel, transport = get_connected_channel(on_send, disconnect_timeout=2)
    with transport, channel:

        channel.expect_transition(OrderedChannel.State.CONNECTING)
        # Create a transaction that times out
        assert_throws(lambda: channel.send('Payload1'.encode()), TransactionTimeout)

        # Then send another and make sure that the seq_number is correct (should be reset to 1)
        channel.send('Payload2'.encode())
        channel.send('Payload3'.encode())
        channel.expect_transition(OrderedChannel.State.DISCONNECTED)
        assert channel.local_seq_number == 2

    data_packets = unique_list([e for e in sent if isinstance(e, DataPacket)])
    assert data_packets[0].seq_number == 1
    assert data_packets[1].seq_number == 1
    assert data_packets[2].seq_number == 2


def test_reconnect_during_transaction():
    sent = []

    connect_done = False
    def on_send(packet: Packet):
        nonlocal connect_done

        packet = transport.parser.parse_ordered(packet)
        sent.append(packet)

        if isinstance(packet, DataPacket):
            if packet.seq_number == 2:

                if not connect_done:
                    # Trigger a reconnect but don't actually send the ack to simulate an ack loss
                    transport.device.set_next_packet(make_payload(make_connect_payload(12, 0, 0), 2, 1, 1, 0))
                    channel.expect_transition(OrderedChannel.State.CONNECTING)
                else:
                    transport.device.set_next_packet(make_payload(make_ack_payload(2), 2, 1, 1, 0))
                return
            else:
                transport.device.set_next_packet(make_payload(make_ack_payload(packet.seq_number), 2, 1, 1, 0))
        elif isinstance(packet, ConnectPacket):
            if not connect_done:
                channel.expect_transition(OrderedChannel.State.CONNECTED)
                connect_done = True

            transport.device.set_next_packet(make_payload(make_connect_payload(packet.connect_id, 0, 0), 2, 1, 1, Packet.Flags.REPLY.value))

    channel, transport = get_connected_channel(on_send, disconnect_timeout=3)
    with transport, channel:
        # Create a transaction to increase the seq_number
        channel.send('Payload1'.encode())

        # Then send another and make sure that the seq_number is correct (should be reset to 1)
        channel.send('Payload2'.encode())
        channel.send('Payload3'.encode())
        channel.expect_transition(OrderedChannel.State.DISCONNECTED)
        assert channel.local_seq_number == 2

    data_packets = unique_list([e for e in sent if isinstance(e, DataPacket)])
    assert data_packets[0].seq_number == 1
    assert data_packets[1].seq_number == 2
    assert data_packets[2].seq_number == 1


def test_send_receive_stress():
    sent = []

    connect_done = False
    def on_send(packet: Packet):
        nonlocal connect_done
        sent.append(transport.parser.parse_ordered(packet))

        if isinstance(sent[-1], DataPacket):
            channel.transport.device.set_next_packet(make_payload(make_ack_payload(sent[-1].seq_number), 2, 1, 1, 0))

    expected_acks = []
    expected_sends = []
    channel, transport = get_connected_channel(on_send)
    with transport, channel:
        seq = 1
        for i in range(0, 100):
            if random.choice([True, False]):
                channel.expect_receive(Packet(make_payload(make_data_payload(f'payload{i}', seq), 2, 1, 1, 0)))
                transport.device.set_next_packet(make_payload(make_data_payload(f'payload{i}', seq), 2, 1, 1, 0))
                expected_acks.append(seq)
                seq += 1
            else:
                channel.send(f'sendpayload{i}'.encode())
                expected_sends.append(f'sendpayload{i}')
        channel.expect_transition(OrderedChannel.State.DISCONNECTED)


    acks = unique_list([e.seq_number for e in sent if isinstance(e, AckPacket)])
    assert acks ==  expected_acks

    sends = unique_list([e.payload.decode() for e in sent if isinstance(e, DataPacket)])
    assert sends == expected_sends

def test_ordered_channel_simple_data_2_fragments():
    payload = ('fragmented' * 25).encode() # 250 bytes

    sent = []
    def on_send(packet: Packet):
        sent.append(transport.parser.parse_ordered(packet))

        # Acknowledge packet
        channel.transport.device.set_next_packet(make_payload(make_ack_payload(sent[-1].seq_number), 2, 1, 1, 0))

    channel, transport = get_connected_channel(on_send)
    with transport, channel:
        channel.expect_transition(OrderedChannel.State.DISCONNECTED)
        channel.send(payload)

    sent = unique_list(sent)
    assert len(sent) == 2
    assert sent[0].fragmented
    assert not sent[1].fragmented

    assert sent[0].payload + sent[1].payload == payload


def test_ordered_channel_simple_data_11_fragments():
    payload = ('fragmented' * 250).encode() # 2500 bytes

    sent = []
    def on_send(packet: Packet):
        sent.append(transport.parser.parse_ordered(packet))

        # Acknowledge packet
        channel.transport.device.set_next_packet(make_payload(make_ack_payload(sent[-1].seq_number), 2, 1, 1, 0))

    channel, transport = get_connected_channel(on_send)
    assert channel.fragment_mtu() == 241
    with transport, channel:
        channel.expect_transition(OrderedChannel.State.DISCONNECTED)
        channel.send(payload)

    sent = unique_list(sent)
    assert len(sent) == 11

    sent_payload = bytes()
    for i, e in enumerate(sent):
        assert e.fragmented == (i != len(sent) - 1)

        sent_payload += e.payload

    assert sent_payload == payload

    assert [e.seq_number for e in sent] == list(range(1, 12))


def test_ordered_channel_fragmented_timeout_and_reconnect():
    payload = ('fragmented' * 25).encode() # 250 bytes

    sent = []
    reconnected = False
    connecting_state_expected = False
    connect_state_expected = False
    def on_send(packet: Packet):
        nonlocal reconnected, connecting_state_expected, connect_state_expected
        packet = transport.parser.parse_ordered(packet)
        sent.append(packet)

        if isinstance(packet, DataPacket):
            # Acknowledge first packet, but not second

            if packet.seq_number in [0, 1]:
                channel.transport.device.set_next_packet(make_payload(make_ack_payload(packet.seq_number), 2, 1, 1, 0))

            else:
                if reconnected:
                    channel.transport.device.set_next_packet(make_payload(make_ack_payload(packet.seq_number), 2, 1, 1, 0))
                else:
                    if not connecting_state_expected:
                        # First attempt, trigger reconnect
                        channel.expect_transition(OrderedChannel.State.CONNECTING)
                        connecting_state_expected = True

        elif isinstance(packet, ConnectPacket):
            # Trigger reconnect
            channel.transport.device.set_next_packet(make_payload(make_connect_payload(packet.connect_id), 2, 1, 1, 0))
            reconnected = True

            if not connect_state_expected:
                channel.expect_transition(OrderedChannel.State.CONNECTED)
                connect_state_expected = True

    channel, transport = get_connected_channel(on_send, disconnect_timeout=2)
    with transport, channel:

        # Sent some dummy payload to increase the sequence number
        channel.send('Dummy'.encode())

        # Try to send a fragmented packet
        try:
            error = None
            channel.send(payload)
        except Exception as e:
            error = e

        assert isinstance(error, TransactionTimeout)

        # Try again
        channel.send(payload)
        channel.expect_transition(OrderedChannel.State.DISCONNECTED)


    data_packets = [e for e in sent if isinstance(e, DataPacket)]
    assert len(data_packets) >= 4

    data_packets = unique_list(data_packets)

    assert len(data_packets) == 4

    assert data_packets[2].payload + data_packets[3].payload == payload
    assert data_packets[0].seq_number == 1
    assert data_packets[1].seq_number == 2
    assert data_packets[2].seq_number == 1
    assert data_packets[3].seq_number == 2

def test_ordered_channel_reconnect_packet_during_fragmented_packet():
    payload = ('fragmented' * 25).encode() # 250 bytes

    sent = []
    reconnected = False
    connecting_state_expected = False
    connect_state_expected = False
    def on_send(packet: Packet):
        nonlocal reconnected, connecting_state_expected, connect_state_expected
        packet = transport.parser.parse_ordered(packet)
        sent.append(packet)

        if isinstance(packet, DataPacket):
            # Acknowledge first packet, but not second

            if packet.seq_number in [0, 1]:
                channel.transport.device.set_next_packet(make_payload(make_ack_payload(packet.seq_number), 2, 1, 1, 0))

            else:
                if reconnected:
                    channel.transport.device.set_next_packet(make_payload(make_ack_payload(packet.seq_number), 2, 1, 1, 0))
                else:
                    if not connecting_state_expected:
                        # First attempt, trigger reconnect
                        channel.expect_transition(OrderedChannel.State.CONNECTING)
                        channel.transport.device.set_next_packet(make_payload(make_connect_payload(12), 2, 1, 1, 0))
                        connecting_state_expected = True

        elif isinstance(packet, ConnectPacket):
            # Trigger reconnect
            channel.transport.device.set_next_packet(make_payload(make_connect_payload(packet.connect_id), 2, 1, 1, Packet.Flags.REPLY.value))
            reconnected = True

            if not connect_state_expected:
                channel.expect_transition(OrderedChannel.State.CONNECTED)
                connect_state_expected = True

    channel, transport = get_connected_channel(on_send, disconnect_timeout=2)
    with transport, channel:

        # Sent some dummy payload to increase the sequence number
        channel.send('Dummy'.encode())

        # Then send a fragmented payload
        channel.send(payload)
        channel.expect_transition(OrderedChannel.State.DISCONNECTED)


    data_packets = [e for e in sent if isinstance(e, DataPacket)]
    assert len(data_packets) >= 4

    data_packets = unique_list(data_packets)

    assert len(data_packets) == 4

    assert data_packets[0].payload.decode() == 'Dummy'
    assert data_packets[2].payload == data_packets[1].payload
    assert data_packets[2].payload + data_packets[3].payload == payload

    assert data_packets[0].seq_number == 1
    assert data_packets[1].seq_number == 2
    assert data_packets[2].seq_number == 1
    assert data_packets[3].seq_number == 2

def test_receive_fragmented_packet_2_fragments():
    payload = ('fragmented' * 25) # 250 bytes
    sent = []

    done = Event()
    def on_send(packet: Packet):
        sent.append(transport.parser.parse_ordered(packet))

        if sent[-1].seq_number == 1:
            transport.device.set_next_packet(make_payload(make_data_payload(payload[channel.fragment_mtu():], 2), 2, 1, 1, 0))
            done.set()

    channel, transport = get_connected_channel(on_send)
    with transport, channel:
        channel.expect_receive(Packet(make_payload(make_data_payload(payload, 1), 2, 1, 1, Packet.Flags.FRAGMENTED.value)))
        transport.device.set_next_packet(make_payload(make_data_payload(payload[:channel.fragment_mtu()], 1), 2, 1, 1, Packet.Flags.FRAGMENTED.value))
        channel.expect_transition(OrderedChannel.State.DISCONNECTED)

        done.wait(30)

    assert len(sent) >= 2
    for e in sent:
        assert isinstance(e, AckPacket)

    assert set([e.seq_number for e in sent]) == set([1, 2])


def test_receive_fragmented_packet_11_fragments():
    payload = ('fragmented' * 250) # 2500 bytes
    sent = []


    done = Event()
    remaining_payload = payload
    last_ack = 0
    def next_packet(seq_number: int):
        nonlocal remaining_payload
        assert remaining_payload

        current_payload = remaining_payload[:channel.fragment_mtu()]
        remaining_payload = remaining_payload[channel.fragment_mtu():]
        return make_payload(make_data_payload(current_payload, seq_number), 2, 1, 1, Packet.Flags.FRAGMENTED.value if remaining_payload else 0)

    def on_send(packet: Packet):
        nonlocal last_ack
        sent.append(transport.parser.parse_ordered(packet))

        if not remaining_payload:
            done.set()
        elif sent[-1].seq_number == last_ack + 1:
            last_ack = sent[-1].seq_number
            transport.device.set_next_packet(next_packet(last_ack + 1))

    channel, transport = get_connected_channel(on_send)
    with transport, channel:
        channel.expect_receive(Packet(make_payload(make_data_payload(payload, 1), 2, 1, 1, Packet.Flags.FRAGMENTED.value)))
        transport.device.set_next_packet(next_packet(1))
        channel.expect_transition(OrderedChannel.State.DISCONNECTED)

        done.wait(30)

    assert len(sent) >= 11
    for e in sent:
        assert isinstance(e, AckPacket)

    assert set([e.seq_number for e in sent]) == set([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11])


def test_reconnect_during_fragmented_receive():
    payload = ('fragmented' * 25) # 250 bytes
    sent = []

    done = Event()
    reconnect_expected = False
    connect_done = False
    def on_send(packet: Packet):
        nonlocal reconnect_expected, connect_done
        packet = transport.parser.parse_ordered(packet)
        sent.append(packet)

        if isinstance(packet, AckPacket):
            if packet.seq_number == 1:
                if connect_done:
                    transport.device.set_next_packet(make_payload(make_data_payload(payload[channel.fragment_mtu():], 2), 2, 1, 1, 0))
                    done.set()
                else:
                    transport.device.set_next_packet(make_payload(make_connect_payload(12), 2, 1, 1, 0))

                    if not reconnect_expected:
                        channel.expect_transition(OrderedChannel.State.CONNECTING)
                        reconnect_expected = True
        elif isinstance(packet, ConnectPacket):
            channel.transport.device.set_next_packet(make_payload(make_connect_payload(packet.connect_id), 2, 1, 1, Packet.Flags.REPLY.value))
            if not connect_done:
                channel.expect_transition(OrderedChannel.State.CONNECTED)
                connect_done = True
                transport.device.set_next_packet(make_payload(make_data_payload(payload[:channel.fragment_mtu()], 1), 2, 1, 1, Packet.Flags.FRAGMENTED.value))

    channel, transport = get_connected_channel(on_send)
    with transport, channel:
        channel.expect_receive(Packet(make_payload(make_data_payload(payload, 1), 2, 1, 1, Packet.Flags.FRAGMENTED.value)))
        transport.device.set_next_packet(make_payload(make_data_payload(payload[:channel.fragment_mtu()], 1), 2, 1, 1, Packet.Flags.FRAGMENTED.value))

        done.wait(30)
        assert done.is_set()
        channel.expect_transition(OrderedChannel.State.DISCONNECTED)

    ack_packets = unique_list([e for e in sent if isinstance(e, AckPacket)])

    assert len(ack_packets) == 2
    assert ack_packets[0].seq_number == 1
    assert ack_packets[1].seq_number == 2

