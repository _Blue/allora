import random
from .mock_device import *
from allora.transport import Transport, BROADCAST_ADDRESS
from allora.encryption import NullEncryption
from allora.packet import Packet, Builder

def make_transport_pair():
    left_device, right_device = get_connected_devices()

    left = Transport(left_device, NullEncryption(), 1)
    right = Transport(right_device, NullEncryption(), 2)

    return left, right


def make_payload(payload: bytes, source: int, destination: int, channel: int, flags=0, ttl=0):
    builder = Builder(NullEncryption())

    return builder.build(payload, source, destination, channel, flags, ttl)

class MockTransport(Transport):
    def __init__(self, address: int, device=None, disable_receive_callback=False, send_callback=None, relay_packets=False, outgoing_ttl=0):
        super().__init__(device if device else MockDevice(), NullEncryption(), address, relay_packets=relay_packets, outgoing_ttl=outgoing_ttl)

        self.expected_sent = []
        self.expected_receives = []

        if not disable_receive_callback:
            self.register_packet_callback(self.on_receive)

        self.device.set_send_callback(send_callback or self.on_send)

    def on_send(self, payload: bytes):
        expected_packet = self.expected_sent[0] if self.expected_sent else None
        assert expected_packet == payload
        del self.expected_sent[0]

    def on_receive(self, packet: Packet):
        assert len(self.expected_receives) > 0
        expected_packet = self.expected_receives[0]

        assert packet == expected_packet
        del self.expected_receives[0]

        return True

    def expect_sent(self, payload: bytes):
        self.expected_sent.append(payload)

    def expect_receive(self, packet: Packet):
        self.expected_receives.append(packet)

    def validate(self):
        if self.running:
            self.sync()

        assert len(self.expected_sent) == 0
        assert len(self.expected_receives) == 0

    def __del__(self):
        self.validate()

    def __enter__(self):
        if self.running:
            return self
        else:
            return super().__enter__()

    def __exit__(self, *args, **kwargs):
        self.sync()
        super().__exit__(*args, **kwargs)

    def sync(self):
        self.device.join()
        super().sync()

def test_basic_transport_messages():
    called = False
    def on_send(payload: bytes):
        nonlocal called
        assert list(payload) == [33, 11, 0, 0, 0, 0, 0, 0, 0, 5, 57]

        called = True
    transport = Transport(MockDevice(), NullEncryption(), 1)
    transport.device.set_send_callback(on_send)

    with transport:
        send_message = int(1337).to_bytes(8, 'big')
        transport.send(send_message, 2, 11, 0)

        received = []
        def on_receive(packet: Packet):
            nonlocal received
            received.append(packet)
            return True

        callback = transport.register_packet_callback(on_receive)
        receive_message = int(1338).to_bytes(4, 'big')
        transport.device.set_next_packet(Builder(NullEncryption()).build(receive_message, 2, 1, 0, 0, 0))

        transport.device.join()
        transport.sync()
        assert len(received) == 1
        assert received[0].source == 2
        assert received[0].destination == 1
        assert received[0].flags == 0
        assert received[0].payload == receive_message

        assert called


def test_multiple_transport_send():
    transport = MockTransport(1)

    with transport:
        payload_1 = '0xcafefade'.encode()
        payload_2 = '0xcafefade2'.encode()
        payload_3 = '0xcafefade3'.encode()

        transport.expect_sent(make_payload(payload_1, 1, 2, 3))
        transport.expect_sent(make_payload(payload_2, 1, 2, 3))
        transport.expect_sent(make_payload(payload_3, 1, 2, 5))

        transport.send(payload_1, 2, 3, 0)
        transport.send(payload_2, 2, 3, 0)
        transport.send(payload_3, 2, 5, 0)
        transport.validate()

        transport.expect_sent(make_payload(payload_3, 1, 2, 4))
        transport.expect_sent(make_payload(payload_1, 1, 3, 3, 12))
        transport.expect_sent(make_payload(payload_2, 1, 4, 5))

        transport.send(payload_3, 2, 4, 0)
        transport.send(payload_1, 3, 3, 12)
        transport.send(payload_2, 4, 5, 0)
        transport.validate()


def test_multiple_transport_receives():
    transport = MockTransport(1)
    with transport:
        payload_1 = '0xcafefade'.encode()
        payload_2 = '0xcafefade2'.encode()
        payload_3 = '0xcafefade3'.encode()

        transport.expect_receive(Packet(make_payload(payload_1, 2, 1, 3)))
        transport.expect_receive(Packet(make_payload(payload_2, 2, 1, 3)))
        transport.expect_receive(Packet(make_payload(payload_3, 2, 1, 3)))

        transport.device.set_next_packet(make_payload(payload_1, 2, 1, 3))
        transport.device.set_next_packet(make_payload(payload_2, 2, 1, 3))
        transport.device.set_next_packet(make_payload(payload_3, 2, 1, 3))
        transport.validate()

        transport.expect_receive(Packet(make_payload(payload_3, 2, 1, 3)))
        transport.expect_receive(Packet(make_payload(payload_2, 2, 1, 3)))
        transport.expect_receive(Packet(make_payload(payload_1, 2, BROADCAST_ADDRESS, 3, 12)))

        transport.device.set_next_packet(make_payload(payload_3, 2, 1, 3))
        transport.device.set_next_packet(make_payload(payload_2, 2, 1, 3))
        transport.device.set_next_packet(make_payload(payload_1, 2, BROADCAST_ADDRESS, 3, 12))

        transport.validate()


def test_receive_address_filtering():
    transport = MockTransport(1)

    with transport:
        payload = 'payload'.encode()

        # Sent from 2 to ourselve
        transport.expect_receive(Packet(make_payload(payload, 2, 1, 3)))
        transport.device.set_next_packet(make_payload(payload, 2, 1, 3))
        transport.validate()


        # Sent to another address, should be dropped
        transport.device.set_next_packet(make_payload(payload, 2, 4, 3))
        transport.validate()
        # Sent to the broadcast address, should be received

        transport.expect_receive(Packet(make_payload(payload, 2, BROADCAST_ADDRESS, 3)))
        transport.device.set_next_packet(make_payload(payload, 2, BROADCAST_ADDRESS, 3))

        transport.validate()

def test_callback_registration():
    payload = 'payload'.encode()

    transport = Transport(MockDevice(), NullEncryption(), 1)
    with transport:
        cb1_count = 0

        def callback_1(packet: Packet):
            nonlocal cb1_count
            cb1_count += 1

            assert packet.payload == payload
            return cb1_count < 2

        cb_1 = transport.register_packet_callback(callback_1)
        transport.device.set_next_packet(make_payload(payload, 2, 1, 3))

        # Validate that cb1 was called once
        transport.device.join()
        transport.sync()
        assert cb1_count == 1

        cb2_count = 0
        def callback_2(packet: Packet):
            nonlocal cb2_count
            cb2_count += 1

            assert packet.payload == payload
            return True

        cb_2 = transport.register_packet_callback(callback_2)
        transport.device.set_next_packet(make_payload(payload, 2, 1, 4))

        # Validate that cb1 was called twice, and cb2 once
        transport.device.join()
        transport.sync()
        assert cb1_count == 2
        assert cb2_count == 1

        # Now remove cb1

        transport.unregister_packet_callback(cb_1)
        transport.device.set_next_packet(make_payload(payload, 2, 1, 4))

        # Validate that cb1 was not called this time
        transport.device.join()
        transport.sync()
        assert cb1_count == 2
        assert cb2_count == 2

        # Now register cb2 and validate that nothing gets called (this triggers a warning)
        transport.unregister_packet_callback(cb_2)
        transport.device.set_next_packet(make_payload(payload, 2, 1, 4))

        transport.device.join()
        transport.sync()
        assert cb1_count == 2
        assert cb2_count == 2

def test_callback_exception_is_caught():
    payload = 'payload'.encode()

    transport = Transport(MockDevice(), NullEncryption(), 1)
    with transport:
        calls = 0
        def callback(packet: Packet):
            nonlocal calls
            calls += 1

            if calls == 1:
                raise RuntimeError('Test error')
            else:
                assert packet.payload.decode() == 'received'
                return True


        transport.register_packet_callback(callback)

        # This should trigger the exception
        transport.device.set_next_packet(make_payload(payload, 2, 1, 4))
        transport.device.join()
        transport.sync()

        # Validate that new packets are still received
        transport.device.set_next_packet(make_payload('received'.encode(), 2, 1, 5))
        transport.device.join()
        transport.sync()
        assert calls == 2

def test_send_receive_stress():
    transport = MockTransport(1)

    sent_count = 0
    received_count = 0
    with transport:
        for _ in range(0, 200):
            action = random.choice([0, 1, 2])
            if action == 0:
                payload = f'SendPayload-{sent_count}'.encode()
                content = make_payload(payload, 1, 2, 4)

                transport.expect_sent(content)
                transport.send(payload, 2, 4, 0)
                send_count =+ 1
            elif action == 1:
                payload = f'ReceivePayload-{received_count}'.encode()
                content = make_payload(payload, 2, 1, 4)
                transport.expect_receive(Packet(content))
                transport.device.set_next_packet(content)
                received_count += 1
            elif action == 2:
                transport.sync()
                transport.validate()

        transport.device.join()
        transport.sync()
        transport.validate()


def test_transport_packet_relayed():
    transport = MockTransport(1, relay_packets=True)
    with transport:
        payload = '0xcafefade'.encode()

        transport.expect_sent(make_payload(payload, 2, 3, 1, flags=Packet.Flags.RELAYED.value, ttl=1))
        transport.device.set_next_packet(make_payload(payload, 2, 3, 1, ttl=2))
        transport.validate()
        assert transport.relayed_packets == 1


def test_transport_2_broadcast_packets_relayed():
    transport = MockTransport(1, relay_packets=True)
    with transport:
        payload = '0xcafefade'.encode()
        payload_2 = '0xcafefade2'.encode()

        transport.expect_sent(make_payload(payload, 2, BROADCAST_ADDRESS, 1, flags=Packet.Flags.RELAYED.value, ttl=1))
        transport.expect_sent(make_payload(payload_2, 2, BROADCAST_ADDRESS, 1, flags=Packet.Flags.RELAYED.value, ttl=1))

        # Relayed broadcast packets should be indicated to L3
        transport.expect_receive(Packet(make_payload(payload, 2, BROADCAST_ADDRESS, 1, flags=Packet.Flags.RELAYED.value, ttl=1)))
        transport.expect_receive(Packet(make_payload(payload_2, 2, BROADCAST_ADDRESS, 1, flags=Packet.Flags.RELAYED.value, ttl=1)))

        transport.device.set_next_packet(make_payload(payload, 2, BROADCAST_ADDRESS, 1, ttl=2))
        transport.device.set_next_packet(make_payload(payload_2, 2, BROADCAST_ADDRESS, 1, ttl=2))

        transport.validate()

        assert transport.relayed_packets == 2


def test_transport_packet_ttl_0_not_relayed():
    transport = MockTransport(1, relay_packets=True)
    with transport:
        payload = '0xcafefade'.encode()

        transport.device.set_next_packet(make_payload(payload, 2, 3, 1, ttl=0))
        transport.validate()

        assert transport.relayed_packets == 0

def test_transport_packet_for_ourselves_not_relayed():
    transport = MockTransport(1, relay_packets=True)
    with transport:
        payload = '0xcafefade'.encode()

        transport.device.set_next_packet(make_payload(payload, 2, 1, 1, ttl=2))
        transport.expect_receive(Packet(make_payload(payload, 2, 1, 1, ttl=2)))
        transport.validate()
        assert transport.relayed_packets == 0


def test_transport_packet_from_ourselves_not_relayed():
    transport = MockTransport(1, relay_packets=True)
    with transport:
        payload = '0xcafefade'.encode()

        transport.device.set_next_packet(make_payload(payload, 1, 2, 1, ttl=2))
        transport.validate()
        assert transport.relayed_packets == 0

def test_transport_outgoing_ttl_is_set():
    transport = MockTransport(1, relay_packets=True, outgoing_ttl=12)
    with transport:
        payload = '0xcafefade'.encode()

        transport.send(payload, 2, 1, 0)
        transport.expect_sent(make_payload(payload, 1, 2, 1, ttl=12))
        transport.validate()
        assert transport.relayed_packets == 0

