from allora.encryption import EncryptionLayer, NullEncryption
from allora.errors import *
from Crypto.Random import get_random_bytes

KEY = get_random_bytes(16)

layer = EncryptionLayer(KEY)

def test_basic_encryption():
    payload = 'payload'.encode()
    cipher = layer.encrypt(payload)

    assert layer.decrypt(cipher) == payload
    assert payload not in cipher

def test_null_encryption():
    null_layer = NullEncryption()

    assert null_layer.decrypt(null_layer.decrypt('payload'.encode())) == 'payload'.encode()

def test_encrypt_big():
    payload = ('payload' * 1000).encode()
    cipher = layer.encrypt(payload)

    assert len(cipher) > len(payload)
    assert layer.decrypt(cipher) == payload
    assert payload not in cipher

def test_empty_encryption():
    payload = bytes()
    cipher = layer.encrypt(payload)

    assert len(cipher) > len(payload)
    assert layer.decrypt(cipher) == payload

def test_encryption_generates_different_ciphers():
    payload = 'dummy'.encode()
    assert layer.encrypt(payload) != layer.encrypt(payload)

def test_decryption_wrong_key():
    payload = 'payload'.encode()
    cipher = layer.encrypt(payload)

    error = None
    try:
        EncryptionLayer(get_random_bytes(16)).decrypt(cipher)
    except Exception as e:
        error = e

    assert isinstance(error, UnauthenticatedPacketException)

def test_decryption_on_corrupted_byte():
    payload = 'payload'.encode()
    cipher = layer.encrypt(payload)

    cipher = bytes([(cipher[0] + 1) % 256]) + cipher[1:]

    error = None
    try:
        layer.decrypt(cipher)
    except Exception as e:
        error = e

    assert isinstance(error, UnauthenticatedPacketException)


def test_decryption_one_removed_byte():
    payload = 'payload'.encode()
    cipher = layer.encrypt(payload)

    error = None
    try:
        layer.decrypt(cipher[1:])
    except Exception as e:
        error = e

    assert isinstance(error, UnauthenticatedPacketException)


def test_decryption_one_extra_byte():
    payload = 'payload'.encode()
    cipher = layer.encrypt(payload)

    error = None
    try:
        layer.decrypt(cipher + bytes([0]))
    except Exception as e:
        error = e

    assert isinstance(error, UnauthenticatedPacketException)
