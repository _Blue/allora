from allora.packet import Packet
from allora.test.test_transport import make_payload



def test_packet_ttl_doesnt_change_flags():
    packet = Packet(make_payload(b'dummy', 0, 1, 12, 0))

    assert packet.source == 0
    assert packet.destination == 1
    assert packet.channel == 12
    assert packet.payload == b'dummy'
    assert packet.ttl == 0
    assert not packet.fragmented

    packet.ttl = 3
    assert packet.source == 0
    assert packet.destination == 1
    assert packet.channel == 12
    assert packet.payload == b'dummy'
    assert packet.ttl == 3
    assert not packet.fragmented

    packet.ttl = 0
    assert packet.source == 0
    assert packet.destination == 1
    assert packet.channel == 12
    assert packet.payload == b'dummy'
    assert packet.ttl == 0
    assert not packet.fragmented

def test_fragmented_packet():
    packet = Packet(make_payload(b'dummy', 0, 1, 12, Packet.Flags.FRAGMENTED.value))

    assert packet.fragmented
    assert packet.ttl == 0

    packet.ttl = 12
    assert packet.fragmented
    assert packet.ttl == 12


def test_packet_set_flags():
    packet = Packet(make_payload(b'dummy', 0, 1, 12, Packet.Flags.FRAGMENTED.value))

    assert packet.fragmented
    assert not packet.relayed
    assert packet.ttl == 0

    packet.flags = Packet.Flags.RELAYED.value

    assert not packet.fragmented
    assert packet.relayed
    assert packet.ttl == 0

    packet.ttl = 2
    packet.flags = Packet.Flags.REPLY.value

    assert packet.flags == Packet.Flags.REPLY.value
    assert packet.ttl == 2
