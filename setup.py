from setuptools import setup, find_packages

setup(
    name="allora",
    version="1",
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'pycryptodome==3.15.0',
        'adafruit-circuitpython-busdevice==5.1.8',
        ]
    )
